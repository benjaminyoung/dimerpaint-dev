# actually crucial

* [x] make saving work
    * [x] saving doesn't remember lengths and show but it should
    
* [x] height functions in graphgen - 2 numbers associated to each edge- matched and not.
	- [ ] [ ] for hex: each edge gets (+2, -1)
	[ ] for square: each edge gets (+3, -1).      
* [ ] make maximize/minimize work (uses graphgen height function info)
* [x] make centers work
* [x] loading different files
* [x] ending a lasso in select mode on a button crashes dimerpaint
* [x] border is broken

# nearly crucial

* [ ] check that everything else still works
* [x] resizing the screen too short crashes and gives a divide by zero error
* [x] boxes only show colors on one side. should be a dot product
* [ ] right clicks crash dimerpaint

# after feature freeze
- [x] Remove all canadian spelling from the code
- [ ] there's old code which highlights a path, I think, that can probably go
- [ ] remove all code that does nothing
- [ ] stop looking *anything* up in coords / dualcoords?? sometimes it seems to be necessary, for drawing purposes to draw things in the correct location - but I don't understand why
- [ ] postscript code should be removed from render code and put in a common location. it should only be when render is clicked
- [ ] is there a smarter place to put the save directory?

# releasing version 1.0 of dimerpaint
- [ ] Figure out how to create binary versions for linux/mac/windows
- [ ] figure out how to structure a git repo so that there's releases in it
- [ ] PR campaign

# would be nice if

* [ ] aztec diamond graphs
* [ ] triple dimer model - could be more complicated
* [x] squish map
* [ ] numbers in selection were little pictures of edges
- [ ] height functions 
	* [ ] in dimerpaint:
		* [ ] read height change data (meaning: height change across edge, with or without a matched edge, white on the right)
		* [ ] find boundary
		* [ ] check if boundary is simply connected
		* [ ] if so: compute heights around boundary
		* [ ] find spot where that boundary height is minimal; make an adjuster for what the height actually is there
		* [ ] fill in the heights iff all of the above worked
		* [ ] maybe do relative height function for double configs
- [ ] Switch to pygame-gui for gui elements
	- [ ] start by using it for buttons
	- [ ]  make the buttons hilight when you mouse over them (didn't do this) 
	- [ ] more normal load/save dialogs and settings dialog
	- [ ] allow arbitrarily many views of the config, in an m by n grid
	- [ ] allow any view to be single-dimer, multiple-dimer, squish, whatever
	- [ ] colors changeable (pygame-gui has a color picker)
- [ ] select a region and then have it minimize or maximize that region 
    - [ ] also if no previous matching was present



# Bugs
* [ ] clicking on the same show/hide button repeatedly makes the screen move
* [x] doubled edges aren't showing in the "both A and B" view
* [x] NameError: "args" is not defined, in render_center_buttons
* [ ] "randomize" is not random - I think it doesn't toggle enough boxes. Start with either min or max configuration and it takes multiple hits of "randomize" to get something farther away from the max or min 
	* [ ] This tends to be actually helpful! often you want a configuration without too many boxes in it.  but yes it doesn't give you a uniformly random config, it just does flips for a while.

