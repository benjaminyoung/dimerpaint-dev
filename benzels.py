import numpy as np

v1 = np.array((0, 2))
v2 = np.array((-3, -1))
v3 = np.array((3, -1))


def benzel_corners(a,b):
    return [
        a*v2 + b*v1,
        -a*v3 - b*v1,
        a*v3 + b*v2,
        -a*v1 - b*v2,
        a*v1 + b*v3,
        -a*v2 - b*v3
    ]



def benzel_cell_coords(a,b):
    c = max(a,b)
    result = set()
    for i in range(-c, c+1):
        for j in range(-c, c+1):
            for k in range(-c, c+1):
                if i+j+k==1 and -(a-1) <= j-i <= (b-1) and -(a-1) <= k-j <= (b-1) and -(a-1) <= i-k <= (b-1) :
                    result.add((i,j,k))
    return result


def benzel(a, start=7):
    shift = np.array((3*a, 0))
    return [(v1*i + v2*j + v3 * k + shift) for (i,j,k) in benzel_cell_coords(start + a, start + 2*a)]

def compress(p):
    x = p[0]//3
    if x<0:
        return p - np.ceil((x-1)/3) * np.array((3,3))
    else:
        return p + np.floor((x+1)/3) * np.array((-3, 3))
    
def compressed_benzel(a, start = 7):
    return [tuple(compress(p)) for p in benzel(a, start = start)]
    
def benzel_segments(a, start = 7):    
    segments = set()
    for p in compressed_benzel(a, start = start):
        corners = [tuple(u) for u in square_centered_at(p)]
        for i in range(4):
            a = corners[i-1]
            b = corners[i]
            slope = (b[1]-a[1])/(b[0]-a[0])
            mx = (a[0] + b[0])             
            if mx < 0:
                if (mx % 12) != 3 or slope != -1:
                    segments.add((corners[i-1], corners[i]))
            elif mx > 0:
                if (mx % 12) != 9 or slope != 1:
                    segments.add((corners[i-1], corners[i]))
    return segments



def benzel_dual_segments(a, start = 7):    
    segments = set()
    compressed_list = compressed_benzel(a, start = start)
    x_min = min(compressed_list)[0] 
    x_max = max(compressed_list)[0]
    y_min = min(compressed_list)[1] 
    y_max = max(compressed_list)[1]
    for p in compressed_list:
        if p[0] == 0:
            segments.add(((p[0] - 3, p[1] + 3), tuple(p)))
            segments.add(((p[0] + 3, p[1] + 3), tuple(p)))
            
        elif p[0] >= x_min and p[1] <= y_max:
            if p[0] < 0:
                segments.add(((p[0] + 3, p[1] + 3), tuple(p)))
                if p[0] % 2 == 0 and p[0] != x_min:
                    segments.add(((p[0] - 3, p[1] + 3), tuple(p)))
                    
            if p[0] > 0:
                segments.add(((p[0] - 3, p[1] + 3), tuple(p)))
                if p[0] % 2 == 0 and p[0] != x_max:
                    segments.add(((p[0] + 3, p[1] + 3), tuple(p)))
               
    return segments

print(benzel_dual_segments(4))

