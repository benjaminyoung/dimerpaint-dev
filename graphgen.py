#!/usr/bin/env python3

# this is a useless comment for testing - bjy

import math
import pygame
import sys
import itertools
import json
from collections import defaultdict
"""
creates the positioned list of vertices for an a x b x c hexagon graph
returns vertex_list, center_list
"""
def hex_vertices(a, b, c, shift_factor = (0,0)):

    factor = math.sqrt(3)/2
    # makes the height and width of the whole grid too big to start
    height = 2*c + a + b
    width = int(1.5*(a + b)+1)

    #creates the whole grid of hex points (square grid for now)
    vertex_list = []
    center_list = []
    for i in range(-width, width+1):
        for j in range(-height, height):
            # shifts every other row left by 1.5
            if j % 2 == 1:
                if i % 3 != 2:
                    vertex_list.append((i - 3/2, j * factor))
                else:
                    center_list.append((i - 3/2, j * factor))
            # non-shifted rows
            elif i % 3 != 2:
                vertex_list.append((i, j * factor))
            elif i % 3 == 2:
                center_list.append((i, j * factor))

    #trims the list of vertices based on the following six inequalities
    trimmed_list = []
    trimmed_list_with_boundary = []
    trimmings = [trimmed_list, trimmed_list_with_boundary]
    for extend in [0,1]:
        for vertex in vertex_list:
            if (vertex[1] <= factor * (1/1.5*vertex[0] + 2*c + 1.5*extend) and # this one controls the height on the left
                vertex[1] <= factor * (-1/1.5*vertex[0] + 2*c + 2*(a - 1) + 1 + 1.5*extend) and
                vertex[0] >= -0.5-extend and 
                vertex[0] <= (a + b - 1)*1.5 + extend and 
                vertex[1] >= factor * (-1/1.5*vertex[0]-1.5*extend) and 
                vertex[1] >= factor * (1/1.5*vertex[0] + 1/3 - (2*b-1)-1.5*extend)
               ):
                trimmings[extend].append(vertex)


    trimmed_center_list = []
    for vertex in center_list:
        if (vertex[1] <= factor * (1/1.5*vertex[0] + 2*c + 2) and # this one controls the height on the left
            vertex[1] <= factor * (-1/1.5*vertex[0] + 2*c + 2*(a - 1) + 1 + 2) and
            vertex[0] >= -0.5 - 1 and 
            vertex[0] <= (a + b + 2)*1.5 - 3 and 
            vertex[1] >= factor * (-1/1.5*vertex[0] - 2) and 
            vertex[1] >= factor * (1/1.5*vertex[0] + 1/3 - (2*b-1) - 2)
           ):
            trimmed_center_list.append(vertex)

    # creates a list of all included x- and y-values
    x_vals = []
    y_vals = []
    for vertex in trimmed_list:
        x_vals.append(vertex[0])
        y_vals.append(vertex[1])

    # shifts the set of vertices down and right so the whole graph is drawn on screen
    shifted_list = []
    shifted_list_with_boundary = []
    shifted_center_list = []
    for vertex in trimmed_list:
        shifted_list.append((vertex[0] - min(x_vals) + 0.5 + shift_factor[0], vertex[1] - min(y_vals) + 0.5 + shift_factor[1]))
    for vertex in trimmed_center_list:
        shifted_center_list.append((vertex[0] - min(x_vals) + 0.5 + shift_factor[0], vertex[1] - min(y_vals) + 0.5 + shift_factor[1]))
    for vertex in trimmed_list_with_boundary:
        shifted_list_with_boundary.append((vertex[0] - min(x_vals) + 0.5 + shift_factor[0], vertex[1] - min(y_vals) + 0.5 + shift_factor[1]))

    return shifted_list, shifted_center_list, shifted_list_with_boundary

"""
creates a square grid of vertices and centers
"""

def square_vertices(x, y, shift_factor = (0,0)):

    height = y
    width = x

    #creates the whole grid of hex points (square grid for now)
    
    vertex_list = []
    vertex_list_with_boundary = []
    vertices = []
    
    center_list = []

    argh = []
    for extend in [0,1]:
        vertex_list = list(itertools.product(range(-extend, width + extend), range(-extend, height + extend)))
        
        shifted_vertex_list = []
        for vertex in vertex_list:
            new_vertex = (vertex[0] + shift_factor[0], vertex[1] + shift_factor[1])
            shifted_vertex_list.append(new_vertex)
            #if vertex[0] != width-1 and vertex[1] != height-1:
                
            if extend == 0:
                center_list.append((new_vertex[0] + 0.5, new_vertex[1] + 0.5))
                if vertex[0] == 0 or vertex[1] == 0:
                    center_list.append((new_vertex[0] - 0.5, new_vertex[1] - 0.5))
            
                center_list.append((-0.5 + shift_factor[0], height - 0.5 + shift_factor[1]))
                center_list.append((width - 0.5 + shift_factor[0], -0.5 + shift_factor[1]))

        argh.append(vertex_list)
        vertices.append(shifted_vertex_list)
        
        
        
    return vertices[0], center_list, vertices[1]

"""
uses the scaling factor to scale the drawn vertices
"""
def scale_vertices(vertex_list, scale_factor):
    return_vertices = []
    for i in vertex_list:
        return_vertices.append(tuple([(i[0])*scale_factor, (i[1])*scale_factor]))

    return return_vertices

""" 
takes in list of UNSCALED vertices and returns a list of edges in the form edge = [(x1,y1), (x2,y2)]
also takes in a scaling factor so that when the edges are created you can stretch them on the screen
"""
def make_box(edge):

    if max(edge[0][1], edge[1][1]) - min(edge[0][1], edge[1][1]) <= 1:
        y_val = 10
        y_min = min(edge[0][1], edge[1][1]) - 5
    else: 
        y_val = max(edge[0][1], edge[1][1]) - min(edge[0][1], edge[1][1])
        y_min = min(edge[0][1], edge[1][1])

    new_box = pygame.Rect(min(edge[0][0], edge[1][0]),
                            y_min,
                            max(edge[0][0], edge[1][0]) - min(edge[0][0], edge[1][0]),
                            y_val)
    return new_box

""" 
creates the edges given a list of vertices by checking if they are a distance of one apart, 
then scales based on the global scaling factor
"""

def make_edges(my_vertices, scale_factor):
    my_edges = []
    for i in range(len(my_vertices)):
        for j in range(i, len(my_vertices)):
            distance = (my_vertices[i][0] - my_vertices[j][0])**2 + (my_vertices[i][1] - my_vertices[j][1])**2
            if distance <= 1.01 and distance >= 0.99:
                my_edges.append(((my_vertices[i][0]*scale_factor, my_vertices[i][1]*scale_factor), (my_vertices[j][0]*scale_factor, my_vertices[j][1]*scale_factor)))


    # creates invisible rectangles around each edge
    my_boxes = []
    for edge in my_edges:
        my_boxes.append(make_box(edge))
        # checks to see if it's a horizontal edge, and if so it makes the click-box
        # 10 pixels taller for ease of clicking

    return my_edges, my_boxes

"""
takes in the list of edges and returns a dictionary where keys are vertices and values are all connected edges
"""
def vertex_edge_dict(edge_list):
    return_dict = {}
    for edge in edge_list:
        vert1 = edge[0]
        vert2 = edge[1]
        if vert1 not in return_dict:
            return_dict[vert1] = [edge]
        else:
            return_dict[vert1].append(edge)
        if vert2 not in return_dict:
            return_dict[vert2] = [edge]
        else:
            return_dict[vert2].append(edge)
    return return_dict

"""
takes in a list of edges and returns a dictionary of vertex: color
"""
def find_bipartite(edge_list, toggle_color = False):
    vertex_dict = vertex_edge_dict(edge_list)
    vertex_list = list(vertex_dict.keys())
    current_vertex = vertex_list[0]
    color_dict = {current_vertex: int(toggle_color)}
    color_list = [[current_vertex, color_dict[current_vertex]]]
    
    angle_dict = defaultdict(list)
    
    active_verts = [current_vertex]
    while len(active_verts) > 0:
        current_vertex = active_verts.pop()
        current_color = color_dict[current_vertex]
        current_edges = vertex_dict[current_vertex]
        for edge in current_edges:
            edge_list = list(edge)
            edge_list.remove(current_vertex)
            if edge_list[0] not in color_dict:
                active_verts.append(edge_list[0])
                color_dict[edge_list[0]] = int(not current_color)
                color_list.append([edge_list[0], int(not current_color)])
            # edge has a black and a white vertex, so orient the edge from 0 to 1, need to check which end is 0
            if color_dict[edge[0]] == 0:
                (a, b) = edge
            else:
                (b, a) = edge
            # now the edge goes from a to b
            angle = math.atan2(b[1] - a[1], b[0] - a[0])
            rounded_angle = round(angle, 4)
            
            angle_dict[rounded_angle].append(edge)
            
    
    return color_list, angle_dict


    

"""
calls the draw circle function in pygame to draw a dots at the location of each vertex in the requested color
"""
def render_vertex(coord_tuple, color, thickness = 4):
    point = (coord_tuple[0], coord_tuple[1])
    pygame.draw.circle(screen, color, point, thickness)

"""
calls the draw line function in pygame to draw a line from coord1 to coord2 in the requested color
"""
def render_edge(coord1, coord2, color, thickness = 4):
    pygame.draw.line(screen, color, coord1, coord2, thickness)
    
    
""" 
checks to see if the given list is a closed loop
"""
def is_closed_loop(edge_list):
    vertex_degree = {}
    for edge in edge_list:
        for vertex in edge:
            if vertex not in vertex_degree:
                vertex_degree[vertex] = 1
            else:
                vertex_degree[vertex] += 1
    
    closed = 0
    for vertex in vertex_degree:
        if vertex_degree[vertex] != 2:
            closed -= 1
            break
    if closed < 0:
        return False
    else:
        return True
            
    

"""
returns a dictionary hexagons where the key is hexagon_center, value = [list of all edges around that center]
"""
def get_faces(center_list, edge_list):
    face_dict = {} #key is the center of the face, values are the list of edges
    for edge in edge_list:
        for center in center_list:
            if ((edge[0][0] - center[0])**2 + (edge[0][1] - center[1])**2 <= (1.05 * scale_factor * scale_factor)) and ((edge[1][0] - center[0])**2 +( edge[1][1] - center[1])**2 <= (1.05 * scale_factor * scale_factor)):
                if center not in face_dict:
                    face_dict[center] = [edge]
                else:
                    face_dict[center].append(edge)

    closed_loop_faces = {}
    for center in face_dict:
        if is_closed_loop(face_dict[center]):
            closed_loop_faces[center] = face_dict[center]
    
    return closed_loop_faces, face_dict

# check if two faces share an edge - if so, there's an edge in the dual graph.
def get_dual_graph_edges(partial_face_dict, edge_list):
    dual_vertex_dict = {}
    for edge in edge_list:
        for center in partial_face_dict:
            if edge in partial_face_dict[center]:
                if edge not in dual_vertex_dict:
                    dual_vertex_dict[edge] = []
                #key = primal edge, value = list of dual vertices
                dual_vertex_dict[edge].append(center)
    
    dual_edge_dict = {}
    for edge in dual_vertex_dict:
        #if len(dual_vertex_dict[edge]) == 2:
        dual_edge_dict[edge] = tuple(dual_vertex_dict[edge])

        #if len(current_faces) >= 2:
        #    unordered_tile_edges = list(set(current_faces[0]).symmetric_difference(set(current_faces[1])))
        #    if len(current_faces) > 2:
        #        print("WHY ARE THERE MORE THAN TWO FACES IN THIS TILE???")
    return dual_edge_dict
            
    

            
        
# tiles in the dual graph are a combination of all two faces that share an edge

"""
draws a button in the desired position with given label
"""
def button(position, label, event, mouse_location):
    font = pygame.font.Font('freesansbold.ttf', 16 * fourk)
    text = font.render(label, True, black, white)
    buttonbox = text.get_rect()
    buttonbox.midleft = position

    screen.blit(text, buttonbox)
    buttonbox.inflate_ip(20,20)
    pygame.draw.rect(screen, black, buttonbox, 1)
    buttonbox.inflate_ip(10,10)
    pygame.draw.rect(screen, (200,200,200), buttonbox, 1)

    if event.type == pygame.MOUSEBUTTONDOWN and buttonbox.collidepoint(mouse_location):
        pygame.draw.rect(screen, (255, 255, 255), buttonbox, 1)
        return True

"""
draws a button with + and - adjusters on the right and left and 
returns +1 or -1 if + or - is clicked
"""
def pmbutton(position, label, event, mouse_location):
    font = pygame.font.Font('freesansbold.ttf', 16 * fourk)
    start_x = position[0]
    start_y = position[1]

    minus = button(position, "-", event, mouse_location)

    buttonbox = font.render("-", False, black, white).get_rect()
    new_x_position = buttonbox[2]
    button((start_x + new_x_position + 30, start_y), label, event, mouse_location)

    buttonbox = font.render(label, False, black, white).get_rect()
    new_x_position += buttonbox[2]
    plus = button((start_x + new_x_position + 60, start_y), "+", event, mouse_location)

    if minus:
        return -1

    if plus:
        return 1

"""
renders the square grid and centers
"""
def render_squares(x,y):
    unscaled_vertex_list, unscaled_center_list, unscaled_big_vertex_list = square_vertices(x,y, shift_factor = (4, 14 * 1/fourk)) 
    
    edge_list, edge_rect_list = make_edges(unscaled_vertex_list, scale_factor)
    
    big_edge_list = make_edges(unscaled_big_vertex_list, scale_factor)[0]
    
    vertex_list = scale_vertices(unscaled_vertex_list, scale_factor)
    center_list = scale_vertices(unscaled_center_list, scale_factor)

    # goes through the edge list to draw the edges
    for j, i in enumerate(big_edge_list):
        tup_1 = i[0]
        tup_2 = i[1]
        render_edge(tup_1, tup_2, blue, 3 * fourk)
    for j, i in enumerate(edge_list):
        tup_1 = i[0]
        tup_2 = i[1]
        render_edge(tup_1, tup_2, black, 3 * fourk)
        #render_edge(tup_1, tup_2, rainbow(j, len(edge_list)))
        # for a rainbow over all the edges, replace the color with 
        "rainbow(j, len(edge_list))"

    # goes through the list of vertices to draw the dots on top of the edges
    for j,i in enumerate(vertex_list):
        render_vertex(i, black, 3 * fourk)

    for i in center_list:
        render_vertex(i, teal, 4 * fourk)

    return vertex_list, center_list, edge_list, big_edge_list

"""
renders the hexagonal grid and centers
"""
def render_hexes(a,b,c):
    unscaled_vertex_list, unscaled_center_list,unscaled_big_vertex_list = \
            hex_vertices(a, b, c, shift_factor = (4,14 * 1/fourk)) # \ by / by |

    edge_list, edge_rect_list = make_edges(unscaled_vertex_list, scale_factor)
    big_edge_list = make_edges(unscaled_big_vertex_list, scale_factor)[0]

    vertex_list = scale_vertices(unscaled_vertex_list, scale_factor)
    big_vertex_list = scale_vertices(unscaled_big_vertex_list, scale_factor)

    center_list = scale_vertices(unscaled_center_list, scale_factor)
    
    for j, i in enumerate(big_edge_list):
        tup_1 = i[0]
        tup_2 = i[1]
        render_edge(tup_1, tup_2, blue, 3 * fourk)
 
    # goes through the edge list to draw the edges
    for j, i in enumerate(edge_list):
        tup_1 = i[0]
        tup_2 = i[1]
        render_edge(tup_1, tup_2, black, 3 * fourk)
        #render_edge(tup_1, tup_2, rainbow(j, len(edge_list)))
        # for a rainbow over all the edges, replace the color with 
        "rainbow(j, len(edge_list))"
        
    # goes through the list of vertices to draw the dots on top of the edges    
    for j,i in enumerate(vertex_list):
        render_vertex(i, black, 3 * fourk)

    for i in center_list:
        render_vertex(i, teal, 4 * fourk)

    return vertex_list, center_list, edge_list, big_edge_list

"""
I WANT IT TO: return a dictionary where each key is the center of a tile (currently either a domino or a rhombus for square grid and hex grid, respectively), and each value is the list of all vertices in that tile
IT DOES: return a list of all possible tiles described above (so a list instead of a dictionary)
TO DO: This has a lot of repeated code from get_faces, see about combining these functions
"""
def get_tiles_old(partial_face_dict, big_edge_list, center_list):
    
    # is a dictionary of [(primal edge)] = [list of the edge in the dual graph that corresponds to that edge]
    adjacency = get_dual_graph_edges(partial_face_dict, big_edge_list)
    tile_dict = {}
    tile_list = []
    
    
    vertex_edge = vertex_edge_dict(big_edge_list)
    
    # loop through the list of edges, saving each vertex as either v1 or v2
    # note: it says "big_edge_list" because we want centers outside the graph proper to have dual adjacency info
    for edge in adjacency:
        v1_adjacent_centers = []
        v2_adjacent_centers = []
        v1_dual_edges = []
        v2_dual_edges = []
        #dual edge
        v1 = edge[0]
        v2 = edge[1]
        
        v1_adjacent_edges = vertex_edge[v1]
        v2_adjacent_edges = vertex_edge[v2]
        
        if len(v1_adjacent_edges) <= 1 or len(v2_adjacent_edges) <= 1:
            continue
        
        for primal_edge in v1_adjacent_edges:
            v1_dual_edges.append(adjacency[primal_edge])
        for primal_edge in v2_adjacent_edges:
            v2_dual_edges.append(adjacency[primal_edge])
        
        unordered_tile_edges = list(set(v1_dual_edges).symmetric_difference(set(v2_dual_edges)))
        
        """
        # go through the list of centers and check whether each one is adjacent to v1 or v2 from the given edge
        for center in center_list:
            #pythagorean theorem
            if ((v1[0] - edge[0][0])**2 + (v1[1] - edge[0][1])**2 <= (1.05 * scale_factor * scale_factor)):
                v1_adjacent_centers.append(edge)
            if ((v2[0] - edge[1][0])**2 + (v2[1] - edge[1][1])**2 <= (1.05 * scale_factor * scale_factor)):
                v2_adjacent_centers.append(edge)
                    
            unordered_tile_edges = list(set(v1_adjacent_centers) | set(v2_adjacent_centers))
        #print(unordered_tile_edges)
        """
        
        
#        start_vertex = unordered_tile_edges[0][0]
        
        ordered_tile_edges = []
        killswitch = 0
        while len(unordered_tile_edges) > 0 and killswitch < 1000:
            if killswitch == 0:
                current_vertex = unordered_tile_edges[0][0]
            for the_edge in unordered_tile_edges:
                if len(the_edge) != 2:
                    ordered_tile_edges.append("WRONG")
                    killswitch = 1000
                    continue
                current_edge = tuple(the_edge)
                if current_vertex in current_edge:
                    ordered_tile_edges.append(current_edge)
                    unordered_tile_edges.remove(current_edge)
                    my_edge = list(current_edge)
                    my_edge.remove(current_vertex)
                    current_vertex = my_edge[0]
            killswitch += 1
        if killswitch == 998:
            print("it died")
        if "WRONG" not in ordered_tile_edges:
            tile_dict[edge] = ordered_tile_edges
            tile_list.append([edge, ordered_tile_edges])

        """
        #adjacency is a dictionary of [(edge)] = [dual vertices surrounding that edge]
        for candidate in adjacency[edge]:
            if (start_vertex not in v1_adjacent_centers) or (start_vertex not in v2_adjacent_centers):
                if (candidate not in v1_adjacent_centers) or (candidate not in v2_adjacent_centers):
                    break

        ordered_tile_vertices = [start_vertex, candidate]
        ordered_tile_edges = [[start_vertex, candidate]]

        while ordered_tile_vertices[-1] != start_vertex:
            work_vertex = ordered_tile_vertices[-1]
            for candidate in adjacency[work_vertex]:
                if (work_vertex not in v1_adjacent_centers) or (work_vertex not in v2_adjacent_centers):
                    if (candidate not in v1_adjacent_centers) or (candidate not in v2_adjacent_centers):
                        if candidate != ordered_tile_vertices[-2]:
                            break
            ordered_tile_vertices.append(candidate)
            ordered_tile_edges.append([work_vertex, candidate]) 
        """

        # tile_dict[edge] = tile_edges
        # tile_list.append(tile_edges)
        #print([edge, ordered_tile_edges])
    
    return tile_list
    # this is currently returning the vertices as lists and not tuples
    
    
def rainbow(i, number_of_colors):
    step_value = int(1530/number_of_colors)
    my_color = 0,0,0
    color_number = int(i * step_value)

    # red to yellow
    if color_number <= 255:
        my_color = 255, color_number, 0

    # yellow to green
    if color_number > 255 and color_number < 510:
        my_color = 255 - (color_number % 255), 255, 0

    # green to cyan
    if color_number >= 510 and color_number < 765:
        my_color = 0, 255, color_number - 510

    # cyan to blue 
    if color_number >= 765 and color_number < 4*255:
        my_color = 0, 255 - (color_number % 255), 255

    # blue to magenta
    if color_number >= 4 * 255 and color_number < 5 * 255:
        my_color = color_number - (4*255), 0, 255

    # magenta to red
    if color_number >= 5 * 255 and color_number < 6 * 255:
        my_color = 255, 0, 255 - (color_number % 255)

    return my_color

##############################################    
######           PYGAME STUFF          #######
##############################################

fourk = 1

scale_factor = 20 * fourk
a = 6
b = 8
c = 4

x_val = 17
y_val = 10

pygame.init()
size = width, height = 900*fourk, 700*fourk

# generate some custom colors since full defaults are garish
black = 0, 0, 0 
white = 255, 255, 255
blue = 100, 100, 255
green = 0, 150, 0
red = 255, 50, 50
purple = 150, 50, 220
teal = 100, 200, 200
magenta = 255, 100, 255
yellow = 255, 255, 50


#renders the screen
screen = pygame.display.set_mode(size, pygame.RESIZABLE)

#sets the background to be white
screen.fill(white)



# needs to be a continual loop to keep drawing the screen
mouse_location = (0,0)
while True:

    for event in pygame.event.get():

        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_location = pygame.mouse.get_pos()

        if event.type == pygame.QUIT: 
            pygame.quit()
            sys.exit()

        square_grid = button((50,50), "square grid", event, mouse_location)
        hexagonal_grid = button((300, 50), "hexagonal grid", event, mouse_location)

        x_size = pmbutton((50, 125), "width", event, mouse_location)
        if x_size == 1 or x_size == -1:
            x_val += x_size
            if x_val < 1:
                x_val = 1

            screen.fill(white)
            vertex_list, center_list, edge_list, big_edge_list = render_squares(x_val, y_val)
            square_grid = True

        y_size = pmbutton((300, 125), "height", event, mouse_location)
        if y_size == 1 or y_size == -1:
            y_val += y_size
            if y_val < 1:
                y_val = 1

            screen.fill(white)
            vertex_list, center_list, edge_list, big_edge_list = render_squares(x_val, y_val)
            square_grid = True

        a_size = pmbutton((50, 200), "a", event, mouse_location)
        if a_size == 1 or a_size == -1:
            a += a_size
            if a < 1:
                a = 1

            screen.fill(white)
            vertex_list, center_list, edge_list, big_edge_list = render_hexes(a,b,c)
            hexagonal_grid = True

        b_size = pmbutton((300, 200), "b", event, mouse_location)
        if b_size == 1 or b_size == -1:
            b += b_size
            if b < 1:
                b = 1
            screen.fill(white)
            vertex_list, center_list, edge_list, big_edge_list = render_hexes(a,b,c)
            hexagonal_grid = True

        c_size = pmbutton((550, 200), "c", event, mouse_location)
        if c_size == 1 or c_size == -1:
            c += c_size
            if c < 1:
                c = 1
            screen.fill(white)
            vertex_list, center_list, edge_list, big_edge_list = render_hexes(a,b,c)
            hexagonal_grid = True

        if square_grid:

            screen.fill(white)
            vertex_list, center_list, edge_list, big_edge_list =  render_squares(x_val, y_val)
            hexagonal_grid = False

            height_change_list = [(tuple(e),(3, -1)) for e in edge_list]

        if hexagonal_grid:

            screen.fill(white)

            vertex_list, center_list, edge_list, big_edge_list = render_hexes(a,b,c)

            height_change_list = [(tuple(e),(2, -1)) for e in edge_list]
            
            square_grid = False
            
        save = button((size[0] - 100, size[1] - 100), "save", event, mouse_location)
        if save:
            face_dict, partial_faces = get_faces(center_list, big_edge_list)
            face_list = list(face_dict.values())
            #tile_dict = get_tiles_old(partial_faces, big_edge_list)
            #tile_list = list(tile_dict.values())
            tile_list = get_tiles_old(partial_faces, big_edge_list, center_list)
            bipartition, angle_dict = find_bipartite(big_edge_list)
            output_dict = {"vertices" : vertex_list,
                           "height_changes": height_change_list,
                           "edges" : edge_list,
                           "dual_vertices" : center_list,
                           "faces" : face_list,
                           "tiles" : tile_list,
                           "matchings" : [{}, {}],
                           "bipartition" : bipartition,
                           "angles" : angle_dict
                           }
            with open("json/lattice.json", "w") as write_file:
                json.dump(output_dict, write_file)
            print("File saved successfully!")
                       
    # display.flip() will update only a portion of the
    # screen to updated, not full area
        pygame.display.flip()




