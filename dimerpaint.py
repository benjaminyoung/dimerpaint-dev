#!/usr/bin/env python3 

import json
import pickle
import copy
import sys
import math
import pygame
import random
import os
import shutil
import shapely.geometry
from collections import defaultdict

print("Python running from", sys.executable)

# Define some colors
black    = (   0,   0,   0)
white    = ( 255, 255, 255)
blue     = (  100,  100, 255)
green    = (  75, 255,   0)
lightgreen  = ( 175, 255,  100)
dkgreen  = (   0, 100,   0)
red      = ( 255,   0,   0)
purple   = (0xBF,0x0F,0xB5)
brown    = (0x55,0x33,0x00)
grey     = (0x7f,0x7f,0x7f) 

# This is a quick hack to make it possible to use poor-quality projectors.
# Edit these lines to alter the colors used.

hi_color = green
selection_color = lightgreen
A_color = black
B_color = blue
center_color = red
Squish_color = purple

fourk = 2

DEBUG = False

def debug(*args):
    if DEBUG:
        print(*args)

def read_vertices_json(vertices):
    vertex_dict = {}
    for vertex in vertices:
        vertex_dict[vertex[0], vertex[1]] = tuple(vertex)
#    debug(vertex_dict)
    return vertex_dict

def squish(coords, ratio = 1):
    w = 0.5
    h = 0.8669254038
    locations = coords.copy()
    xvalues = [v[0] for v in coords.keys()]
    yvalues = [v[1] for v in coords.keys()]
    xmin = min(xvalues)
    ymin = min(yvalues)
    xmax = max(xvalues)
    ymax = max(yvalues)
    for i in locations:
        #print(i[0], i[1])
        if xmin % 24 == 12:
            x_val = i[0]
        else:
            x_val = i[0] - xmin
        x_val = round(x_val/5) + 12
        y_val = i[1] - ymin
        y_val = round(y_val/(5*h))/2
        #if (ymin % 8 == 0) or : # UP RIGHT
        y_val += 4
        #print(xmin, xmax, ymin, ymax)
        
        temp_list = list(locations[i]) 
        if x_val % 24 == 0 and y_val % 8 == 0: # UP RIGHT
                temp_list[0] += ratio*w
                temp_list[1] += ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 0  and y_val % 8 == 4: # DOWN RIGHT
                temp_list[0] += ratio*w
                temp_list[1] -= ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 12 and y_val % 8 == 4: # UP RIGHT
                temp_list[0] += ratio*w
                temp_list[1] += ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 12 and y_val % 8 == 0: # DOWN RIGHT
                temp_list[0] += ratio*w
                temp_list[1] -= ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 2 and y_val % 8 == 2: # RIGHT
                temp_list[0] += ratio
                locations[i] = tuple(temp_list)
        if x_val % 24 == 14 and y_val % 8 == 6: # RIGHT
                temp_list[0] += ratio
                locations[i] = tuple(temp_list)
        if x_val % 24 == 6 and y_val % 8 == 6: # LEFT
                temp_list[0] -= ratio
                locations[i] = tuple(temp_list)
        if x_val % 24 == 18 and y_val % 8 == 2: # LEFT
                temp_list[0] -= ratio
                locations[i] = tuple(temp_list)
        if x_val % 24 == 8 and y_val % 8 == 4: # UP LEFT
                temp_list[0] -= ratio*w
                temp_list[1] += ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 8 and y_val % 8 == 0: # DOWN LEFT
                temp_list[0] -= ratio*w
                temp_list[1] -= ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 20 and y_val % 8 == 0: # UP LEFT
                temp_list[0] -= ratio*w
                temp_list[1] += ratio*h
                locations[i] = tuple(temp_list)
        if x_val % 24 == 20 and y_val % 8 == 4: # DOWN LEFT
                temp_list[0] -= ratio*w
                temp_list[1] -= ratio*h
                locations[i] = tuple(temp_list)
        #print("(", x_val, ",", int(y_val), ")")
    return locations

def is_positively_oriented(pointlist):
    # see stackoverflow.com/questions/1165647/
    # so cool
    acc = 0
    print("looking at", pointlist)
    print("as a set that is", set(pointlist))
    for i in range(len(pointlist)):
        (x2, y2) = pointlist[i]
        (x1, y1) = pointlist[i-1]
        acc += (x2-x1)*(y2+y1)
    return (acc >=0)

def edge_list_to_polygon(edgelist):
    L = cycle_decomposition(edgelist)
    assert len(L) == 1, "This edge list was meant to have just one cycle, but has more:" +str(edgelist)
    return L[0]

# Rescale the x and y coordinates isometrically so that the width of the 
# entire array is the specified size.
def rescale(coords, dualcoords, desired_width, squish_coords):
    xvalues = [v[0] for v in coords.values()]
    yvalues = [v[1] for v in coords.values()]
    xmin = min(xvalues)
    ymin = min(yvalues)
    xmax = max(xvalues)
    ymax = max(yvalues)
    scale_factor = desired_width / (xmax - xmin) 
    for k in coords.keys():
        v = coords[k]
        new_x = round((v[0] - xmin) * scale_factor)
        new_y = round((ymax - v[1]) * scale_factor)
        coords[k] = (new_x, new_y)
    for k in squish_coords.keys():
        v = squish_coords[k]
        new_x = round((v[0] - xmin) * scale_factor)
        new_y = round((ymax - v[1]) * scale_factor)
        squish_coords[k] = (new_x, new_y)
    for k in dualcoords.keys():
        v = dualcoords[k]
        new_x = round((v[0] - xmin) * scale_factor)
        new_y = round((ymax - v[1]) * scale_factor)
        dualcoords[k] = (new_x, new_y)


# Read in a list of edges.  Store in a dictionary.  Each edge is a "frozen set" 
# which is an immutable, unordered tuple
def read_edges_json(edges):
    result = {}
    for edge in edges:
        new_edge = frozenset((tuple(edge[0]), tuple(edge[1])))
        result[new_edge] = 1
    return result

# Read in a list of faces.  Store in a list.
def read_faces_json(faces):
    result = []
    for face in faces:
        new_face = []
        for edge in face:
            new_face.append( (tuple(edge[0]), tuple(edge[1])) )
        new_face = tuple(new_face)
        result.append(new_face)
    return faces
    

def read_tiles_json(tiles):
    tile_dict = {}
    for tile in tiles:
#        debug(tile)
        # FIX THIS IN GRAPHGEN - it should be taking in a tuple
        edge = frozenset((tuple(tile[0][0]), tuple(tile[0][1])))
        rhombus = []
        for dual_edge in tile[1]:
            rhombus.append(tuple(tuple(u) for u in dual_edge))
        debug(edge)
        for dual_edge in tuple(rhombus):
            debug("--->", dual_edge)
        debug("=====")
        tile_dict[edge] = tuple(rhombus)
        
    return tile_dict

def read_bipartition_json(bipartition_list):
    bipartition = {}
    for ((x_pos, y_pos), color) in bipartition_list:
        bipartition[(x_pos, y_pos)] = color
    return bipartition

def read_angles_json(angles_dict):
    result = {}
    for (angle, edge_list) in angles_dict.items():
        edge_set = set()
        for e in edge_list:
            (a, b) = e
            edge_set.add(frozenset((tuple(a), tuple(b))))
        result[float(angle)] = edge_set
    debug(result)
    return result

def read_matchings_json(matchings):
    result = []
    for m in matchings:

        m_as_dict = {frozenset(tuple(endpoint) for endpoint in e): 1 for e in m}
        result.append(m_as_dict)
    return result

def render_hexagon_center(coords, h, xoffset, yoffset, radius, eps):
    debug("Rendering hex center")
    print(h)
    # This is a hack but I just want it to work - average the coordinates of the vertices to get the coordinates of the center.

    acc_x = 0
    acc_y = 0

    for e in h:
        for vertex in e:
            
            (x,y) = coords[tuple(vertex)]
            acc_x += x
            acc_y += y


    center_x = acc_x / 2 / len(h) + xoffset
    center_y = acc_y / 2 / len(h) + yoffset

# I'm supposed to render these in eps I guess?  Cant think why right now
    return pygame.draw.circle(screen, center_color, (center_x,center_y), radius)


# Draw an edge in a given color
def render_edge(coords, edge, xoffset, yoffset, color, width, eps):
    e = [endpt for endpt in edge]
    n = normal(coords, edge, width/2.0)

    p0 = coords[e[0]] 
    p1 = coords[e[1]] 
    p2 = coords[e[1]] 
    p3 = coords[e[0]] 

    eps["coords"].extend([p0, p1]) 
    ps = "%.3f %.3f %.3f setrgbcolor " %(color[0] / 255.0, color[1] / 255.0, color[2] / 255.0) 
    ps += "%.3f setlinewidth newpath %.3f %.3f moveto %.3f %.3f lineto stroke" % (width, p0[0], p0[1], p1[0], p1[1])
    eps["ps"].append(ps)

    p0 = (p0[0] + xoffset + n[0], p0[1] + yoffset + n[1])
    p1 = (p1[0] + xoffset + n[0], p1[1] + yoffset + n[1])
    p2 = (p2[0] + xoffset - n[0], p2[1] + yoffset - n[1])
    p3 = (p3[0] + xoffset - n[0], p3[1] + yoffset - n[1])
    
    #return pygame.draw.line(screen, color, p0, p1, width)
    return pygame.draw.polygon(screen, color, [p0,p1,p2,p3], 0)

def render_line(coords, edge, xoffset, yoffset, color, width, eps):
    e = [endpt for endpt in edge]
    p0 = coords[e[0]] 
    p1 = coords[e[1]] 

    eps["coords"].extend([p0,p1])
    ps = "%.3f %.3f %.3f setrgbcolor " % (color[0] / 255.0, color[1] / 255.0, color[2] / 255.0)
    ps += "%.3f setlinewidth newpath %.3f %.3f moveto %.3f %.3f lineto stroke" % (width, p0[0], p0[1], p1[0], p1[1])
    eps["ps"].append(ps)

    p0 = (p0[0] + xoffset, p0[1] + yoffset)
    p1 = (p1[0] + xoffset, p1[1] + yoffset)


    return pygame.draw.line(screen, color, p0, p1, width)


# given an edge, find a polygon
def locate_rhombus(dualcoords, rhomb, xoffset, yoffset):
    coordslist = []
    work_vertex = rhomb[0][0]
    for e in rhomb:
        coordslist.append(dualcoords[work_vertex])
        work_edge = list(e)
        work_edge.remove(work_vertex)
        work_vertex = work_edge[0]
    shiftedcoordslist = []
    for p0 in coordslist:
        p0 = (p0[0] + xoffset, p0[1] + yoffset)
        shiftedcoordslist.append(p0)
    return coordslist, shiftedcoordslist



def render_rhombus(dualcoords, rhomb, xoffset, yoffset, color, width, eps):
    coordslist, shiftedcoordslist = locate_rhombus(dualcoords, rhomb, xoffset, yoffset)
    pygame.draw.polygon(screen, color, shiftedcoordslist, width)
    eps["coords"].extend(coordslist) 
    flatlist = []
    for p in coordslist:
        flatlist.extend([p[0],p[1]])
    ps = "%.3f %.3f %.3f setrgbcolor " % (color[0] / 255.0, color[1] / 255.0, color[2] / 255.0)

    ps += "newpath %.3f %.3f moveto " % coordslist[-1]  
    for p in coordslist:
        ps += "%.3f %.3f lineto " % p
    ps += "closepath"
    if width==0:
        ps += " fill"
    else:
        ps = ("%.3f setlinewidth " % width) + ps + " stroke"
    eps["ps"].append(ps)

# Compute an integer normal vector to an edge of a given magnitude.
# The edge can be implemented as a list, tuple or set of points, of length 2.
def normal(coords, edge, magnitude):
    e = [endpt for endpt in edge]
    p0 = coords[e[0]] 
    p1 = coords[e[1]] 
    #p0 = e[0]
    #p1 = e[1]
    normal_x = float(p1[1] - p0[1])
    normal_y = float(p0[0] - p1[0])
    normal_length = math.sqrt(normal_x*normal_x + normal_y*normal_y)
    normal_x *= magnitude/normal_length
    normal_y *= magnitude/normal_length
    return (normal_x, normal_y)

def render_double_edge(coords, edge, xoffset, yoffset, color, width, eps):
    e = [endpt for endpt in edge]
    p0 = coords[e[0]] 
    p1 = coords[e[1]] 

    norm = normal(coords, edge, width*2/3) 

    pts = [  (p0[0]  + norm[0], p0[1]  + norm[1]),
             (p1[0]  + norm[0], p1[1]  + norm[1]),
             (p0[0]  - norm[0], p0[1] - norm[1]),
             (p1[0]  - norm[0], p1[1] - norm[1]), ]

    eps["coords"].extend(pts) 
    ps = "%.3f %.3f %.3f setrgbcolor "%(color[0][0]/255.0,color[0][1]/255.0,color[0][2]/255.0) 
    ps += "%d setlinewidth newpath %.3f %.3f moveto %.3f %.3f lineto stroke" % (width,pts[0][0],
            pts[0][1],pts[1][0],pts[1][1])
    eps["ps"].append(ps)
    ps = "%.3f %.3f %.3f setrgbcolor "%(color[1][0]/255.0,color[1][1]/255.0,color[1][2]/255.0) 
    ps += "%.3f setlinewidth newpath %.3f %.3f moveto %.3f %.3f lineto stroke" % (width,pts[2][0],
            pts[2][1],pts[3][0],pts[3][1])
    eps["ps"].append(ps)

    norm = normal(coords, edge, width/2) 
    draw_these = [[pts[0],pts[1]],[pts[2],pts[3]]]
    boxes = []
    for i in range(2):
        edge = draw_these[i]
        poly = [
            (edge[0][0] + xoffset + norm[0], edge[0][1] + yoffset + norm[1]),
            (edge[1][0] + xoffset + norm[0], edge[1][1] + yoffset + norm[1]),
            (edge[1][0] + xoffset - norm[0], edge[1][1] + yoffset - norm[1]),
            (edge[0][0] + xoffset - norm[0], edge[0][1] + yoffset - norm[1]), ]
    
        boxes += pygame.draw.polygon(screen, color[i], poly, 0)


    return boxes

    
def render_vertex(coords, v, xoffset, yoffset, fill_color, boundary_color, radius):
    p = (coords[v][0] + xoffset, coords[v][1] + yoffset)
    pygame.draw.circle(screen, fill_color, p, radius)
    pygame.draw.circle(screen, boundary_color, p, radius, 2)

# Draw the background.  Return bounding boxes, tagged by "A" or "B".
def render_background(renderables, xoffset, yoffset, which_side, eps):
    coords = renderables["coords"] 
    # Turns out that it's nicer to render edges where all the rhombi are.
    #graph = renderables["background"]  
    graph = renderables["rhombi"]
    boundingboxes = []
    for e in graph.keys():
        bb = render_line(coords, e, xoffset, yoffset, grey, 1, eps)        
        bb.inflate_ip(4,4) # make it a little bigger for ease of clicking
        boundingboxes.append(("edge", e, which_side, bb))
    return boundingboxes    

def render_squish_background(renderables, xoffset, yoffset, which_side, eps):
    squish_coords = renderables["squish_coords"] 
    # Turns out that it's nicer to render edges where all the rhombi are.
    #graph = renderables["background"]  
    graph = renderables["rhombi"]
    lengths = renderables["lengths"]
    squish_coords = squish(renderables["coords"], lengths["squish_ratio"])
    boundingboxes = []
    for e in graph.keys():
        bb = render_line(squish_coords, e, xoffset, yoffset, grey, 1, eps)        
#        bb.inflate_ip(4,4) # make it a little bigger for ease of clicking
        boundingboxes.append(("edge", e, which_side, bb))
    return boundingboxes   

# Draw a matching.
def render_matching(renderables, which_side, xoffset, yoffset, color, eps):
    coords = renderables["coords"] 
    matchings = renderables["matchings"]
    lengths = renderables["lengths"]
    boundingboxes = []
    for e in matchings[which_side].keys():
        bb = render_edge(coords, e, xoffset, yoffset, color, lengths["dimer_width"], eps)        
        bb.inflate_ip(2,2) # make it a little bigger for ease of clicking
        boundingboxes.append(("matchededge", e, which_side, bb))
    return boundingboxes

def render_squish_matching(renderables, which_side, xoffset, yoffset, color, eps):
#    squish_coords = renderables["squish_coords"] 
    matchings = renderables["matchings"]
    lengths = renderables["lengths"]
    squish_coords = squish(renderables["coords"], lengths["squish_ratio"])
    boundingboxes = []
    for e in matchings[which_side].keys():
        bb = render_edge(squish_coords, e, xoffset, yoffset, color, lengths["dimer_width"], eps)        
        bb.inflate_ip(2,2) # make it a little bigger for ease of clicking
        boundingboxes.append(("matchededge", e, which_side, bb))
    return boundingboxes

# Draw the vertices, colored according to bipartition, of a matching.
def render_matched_vertices(renderables, which_side, xoffset, yoffset, edge_color, eps):
    coords = renderables["coords"] 
    matchings = renderables["matchings"]
    lengths = renderables["lengths"]
    bipartition = renderables["bipartition"]
    boundingboxes = []
    for e in matchings[which_side].keys():
        for v in e:
            if bipartition[v] == 0:
                color = white
            else:
                color = edge_color
            render_vertex(coords, v, xoffset, yoffset, color, edge_color, 1.5*lengths["dimer_width"])        



# Draw the box pile corresponding to a matching.  This is basically like
# drawing the tiling and then shading the tiles.
def render_boxes(renderables, which_side, xoffset, yoffset, color, eps):
    dualcoords = renderables["dualcoords"]
    rhombi = renderables["rhombi"]
    matchings = renderables["matchings"]
    try:
        global_intensity = renderables["lengths"]["shading_intensity"]
    except KeyError:
        global_intensity = 1

    for edge in matchings[which_side].keys():
        try:
            rhomb = rhombi[edge]
            ends = [p for p in edge]
            a = ends[0]
            b = ends[1]
            if a[1] - b[1] == 0:
                inverseslope = 1
            else:    
                inverseslope = (a[0]-b[0]) / (a[1]-b[1])
            if inverseslope < 0:
                intensity = 0.65
            elif inverseslope < 1 and inverseslope > 0:
                intensity = 0.9
            elif inverseslope == 1:
                intensity = 0.75
            elif round(inverseslope, 3) == 0:
                if a[1] > b[1]:
                    intensity = 0.5
                else: intensity = 0.35
            else: 
                intensity = 0.25
            #intensity = {-1: 0.65, 0:0.9, 1:0.75}[inverseslope]
            fillcolor_components = [0,0,0]
            for i in range(3):
                fillcolor_components[i] = 255 * intensity  + color[i] * (1-intensity)
                fillcolor_components[i] = 255 * (1-global_intensity) + fillcolor_components[i] * global_intensity 
                if fillcolor_components[i] < 0:
                    fillcolor_components[i] = 0
            fillcolor = tuple(fillcolor_components)
            render_rhombus(dualcoords, rhomb, xoffset, yoffset, fillcolor, 0, eps)        
        except KeyError:
            pass

# Draw the tiling corresponding to a matching.
def render_tiling(renderables, which_side, xoffset, yoffset, color, eps):
    dualcoords = renderables["dualcoords"]
    rhombi = renderables["rhombi"]
    matchings = renderables["matchings"]
    try:
        width = renderables["lengths"]["tile_edge_width"]
    except KeyError:
        width = 2
        renderables["lengths"]["tile_edge_width"] = width
    for edge in matchings[which_side].keys():
        try:
            rhomb = rhombi[edge]
            render_rhombus(dualcoords, rhomb, xoffset, yoffset, color, width, eps)        
        except KeyError:
            pass


def render_highlight(renderables, which_side, xoffset, yoffset, color, eps):
    coords = renderables["coords"]
    for e in renderables["highlight"][which_side]:
        render_edge(coords, e, xoffset, yoffset,color, 2*renderables["lengths"]["dimer_width"], eps)

def render_selection(renderables, which_side, xoffset, yoffset, color, eps):
    coords = renderables["coords"]
    for e in renderables["selection"]:
        render_edge(coords, e, xoffset, yoffset,color, 2*renderables["lengths"]["dimer_width"], eps)

# given a graph as adjacency dict, find a cycle.  
# Note: I'm assuming this is a graph I CAN find a cycle in just by walking: nonempty, no leaves, no isolated vertices, etc
def find_cycle(adj):
    cycles = []
    if len(adj) == 0:
        raise Exception("Tried to find a cycle in the empty graph")

    path = [next(iter(adj))] #start a path
    # take the first step
    neighbors = list(adj[path[-1]])
    if len(neighbors) <= 1:
        raise Exception("I found a leaf or disconnected vertex on the zeroth step")
    for v in neighbors: # just take the first step possible
        path.append(v)
        break

    # start walking down the path in the graph. When I self-intersect, return the cycle
    while len(path) <= len(adj):
        neighbors = list(adj[path[-1]])
        next_step = None
        for v in neighbors: # take the first step possible, but now dont backtrack
            if v != path[-2]:
                next_step = v
                break
        if next_step == None: 
            raise Exception("got stuck finding a cycle")
        if next_step in path: # aha! a cycle!
            idx = path.index(next_step)
            cycle =path[idx:] 
            if not is_positively_oriented(cycle):
                cycle = list(reversed(cycle))
            return cycle
        else:
            path.append(next_step)
    raise Exception("I've been walking in this graph for a long time and have not found a cycle")

# given a list of undirected edges, collect them into cycles
def cycle_decomposition(edgelist):
    adj = {}
    for e in edgelist:
        (a,b) = [tuple(u) for u in e]
        for v in (a,b): 
            if v not in adj:
                adj[v] = set()
        adj[a].add(b)
        adj[b].add(a)
    cycles = []
    active_vertices = list(adj.keys())
    while len(active_vertices) > 0:
        new_cycle = find_cycle(adj)    
        cycles.append(new_cycle)
        for i in range(len(new_cycle)):
            a = new_cycle[i]
            b = new_cycle[i-1] #notice sneaky wrap around to end of list
            if a in adj:
                adj[a].discard(b)
            if b in adj:
                adj[b].discard(a)
            if len(adj[a]) == 0:
                del adj[a]
            if len(adj[b]) == 0:
                del adj[b]
        old_active_vertices = active_vertices
        active_vertices = list(adj.keys())
        if len(old_active_vertices) == len(active_vertices):
            raise Exception("tried to find a cycle and it didn't work!")
    return cycles


# Draw the boundary of the matched region.  Note: for now, let's just make it draw the tiles.
def render_boundary(renderables, which_side, xoffset, yoffset, color, eps):
    dualcoords = renderables["dualcoords"]
    rhombi = renderables["rhombi"]
    matchings = renderables["matchings"]
    try:
        width = renderables["lengths"]["tile_edge_width"]
    except KeyError:
        width = 2
        renderables["lengths"]["tile_edge_width"] = width
    borderedges = set()
    for edge in matchings[which_side].keys():
        try:
            rhomb = rhombi[edge]
            coordslist, shiftedcoordslist = locate_rhombus(dualcoords, rhomb, xoffset, yoffset)
            for i in range(len((shiftedcoordslist))):
                j = (i+1) % len(shiftedcoordslist)
                e = frozenset([shiftedcoordslist[i], shiftedcoordslist[j]])
                if e in borderedges:
                    borderedges.discard(e)
                else:
                    borderedges.add(e)
        except KeyError:
            pass
    cycles = cycle_decomposition(borderedges)
    for cycle in cycles:
        pygame.draw.polygon(screen, color, cycle, width)
    




# Draw the boundary of the matched region.  There's a cheap way to do this:
# the boundary is all the edges in the dual graph that are singly covered by
# a tile edge.
# Note: This is old, broken code. 
#def render_boundary(renderables, which_side, xoffset, yoffset, color, eps):
#    dualcoords =renderables["dualcoords"]
#    rhombi =renderables["rhombi"]
#    matchings =renderables["matchings"]
#    rhomb_edges = defaultdict(int)
#    try:
#        width = renderables["lengths"]["tile_edge_width"]
#    except KeyError:
#        width = 2
#        renderables["lengths"]["tile_edge_width"] = width
#
#    for edge in matchings[which_side].keys():
#        try:
#            rhomb = rhombi[edge]
#            for i in range(4):
#                j = (i+1) % 4
#                rhomb_edge = frozenset([rhomb[i], rhomb[j]])
#                rhomb_edges[rhomb_edge] += 1 
#        except KeyError:
#            pass
#    for edge in rhomb_edges.keys():
#        if rhomb_edges[edge] == 1:
#            render_line(dualcoords, edge, xoffset, yoffset, color, width, eps)

# Draw doubled edges in a pair of matchings.
def render_doubled_edges(renderables, xoffset, yoffset, colors, eps):
    coords = renderables["coords"] 
    matchings = renderables["matchings"]
    lengths = renderables["lengths"]
    m1 = matchings[0]
    m2 = matchings[1]
    for e in m1.keys():
        if(e in m2):
            render_double_edge(coords, e, xoffset, yoffset, colors, lengths["dimer_width"],eps)

# Draw edges that are not doubled in a pair of matchings.
def render_xor_edges(renderables, xoffset, yoffset, colors,eps):
    coords = renderables["coords"]
    matchings = renderables["matchings"]
    lengths = renderables["lengths"]
    boxes = []
    m0 = matchings[0]
    m1 = matchings[1]
    for e in m0.keys():
        if e not in m1:
            bb = render_edge(coords, e, xoffset, yoffset, colors[0], lengths["dimer_width"],eps)     
            boxes.append(("matchededge", e, 0, bb))
    for e in m1.keys():
        if e not in m0:
            bb = render_edge(coords, e, xoffset, yoffset, colors[1], lengths["dimer_width"],eps)
            boxes.append(("matchededge", e, 1, bb))
    return boxes


#===================================================================
# Create a button, with given text and center, and a callback function
# for what to do when it's clicked.  Callbacks get a dict, "args", as
# their arguments.  I make no attempt to police what goes into args.
#
# For alignment reasons "pos" is the middle of the left side of the button.

def drawbutton(pos, label, font, callback, args, background_color):
    try:
        text = font.render(label, True, black, background_color)
        bb = text.get_rect()
    except TypeError:
        debug("Here's my line:", label)
        text = pygame.Surface((16, 14), 0, background_color)
        text.fill(background_color)
        text_surface = pygame.draw.line(text, green, label[0], label[1], 4)
        pygame.draw.line(text, black, label[0], label[1], 2)
        pygame.draw.circle(text, black, label[1], 2.5)
        pygame.draw.circle(text, background_color, label[0], 2)
        pygame.draw.circle(text, black, label[0], 2.5, 1)
        bb = text.get_rect()
    bb.topleft = pos
    bb.height = 13*fourk
    border_bb = bb.inflate(4*fourk,4*fourk)
    pygame.draw.rect(screen, background_color, border_bb)
    pygame.draw.rect(screen, black, border_bb, 1)
    screen.blit(text, bb)
    return ("button", args, callback, bb)


# Draw a row of buttons.  Return a list of their bounding boxes.
# Argumnet is a list of pairs: (name, callback, args)

def draw_button_row(x, y, spacing, font, buttondata):
    spacing *= fourk
    boundingboxes = []
    x_current = x
    for button in buttondata:
        pos = (x_current, y)
        bb = drawbutton(pos, button[0], font, button[1], button[2], button[3])
        x_current += bb[3].width + spacing
        boundingboxes.append(bb)
    return boundingboxes

# Draw a plus/minus button for adjusting lengths.
def draw_adjuster(pos, label, quantity, amount, lengths,font,background_color, minimum=0):
    buttons = [
        ("-"+str(amount), adjust_callback, {"quantity":quantity, "amount":-amount, "lengths":lengths, "min":minimum},background_color),
        (label, null_callback, {},background_color),
        ("+"+str(amount), adjust_callback, {"quantity":quantity, "amount":amount,"lengths":lengths, "min":minimum},background_color),
        ]
    return draw_button_row(pos[0], pos[1], 3, font, buttons)

# Draw a row of plus/minus buttons for adjusting lengths.
# The last argument is a list of tuples: (label, quantity, amount)
def draw_adjuster_row(x, y, spacing, lengths, font, adjusterlist):
    spacing *= fourk
    boundingboxes = []
    x_current = x + 2*spacing
    for adjuster in adjusterlist:
        pos = (x_current, y)
        bb = draw_adjuster(pos, adjuster[0], adjuster[1], adjuster[2], lengths, font, adjuster[3]) 
        x_current += bb[0][3].width + bb[1][3].width + bb[2][3].width + 10*fourk + spacing
        boundingboxes.extend(bb)
    return boundingboxes


def test_callback(args):
    debug("Clicked test button on side" , args["side"])

def maximize_callback(args):
    debug("Clicked minimize button on side", args["side"])
    maximize(args["matching"], args["bipartition"], args["height_changes"], args["hexagons"])

def minimize_callback(args):
    debug("Clicked minimize button on side", args["side"])
    minimize(args["matching"], args["bipartition"], args["height_changes"], args["hexagons"])

def randomize_callback(args):
    debug("Clicked randomize button on side", args["side"])
    randomize(args["matching"], args["hexagons"], args["steps"])

def adjust_callback(args):
    lengths = args["lengths"]
    quantity = args["quantity"]
    amount = args["amount"]
    lengths[quantity] += amount
    if(lengths[quantity] < args["min"]):
        lengths[quantity] = args["min"]
    debug("%s set to %f" % (quantity, lengths[quantity]))

def null_callback(args):
    pass

def fullscreen_callback(args):
    return;
    renderables = args["renderables"]

    if "old_screen_size" in renderables["lengths"]:
        pygame.display.set_mode(renderables["lengths"]["old_screen_size"], pygame.RESIZABLE)
        compute_picture_sizes(renderables)
        del renderables["lengths"]["old_screen_size"]
    else:
        modes = pygame.display.list_modes()
        if modes:
            renderables["lengths"]["old_screen_size"] = screen.get_size()
            pygame.display.set_mode(modes[0], pygame.FULLSCREEN)
            compute_picture_sizes(renderables)



def clear_highlight_callback(args):
    debug("Clear all highlighting.")
    args["renderables"]["highlight"] = [{},{}]

def quit_callback(args):
    debug("Quit button clicked.")
    pygame.event.post(pygame.event.Event(pygame.QUIT, {}))

def json_load_callback(args):
    myname = args["myname"]
    f = args["filenames"]
    saved_data = args["saved_data"] # update this
    debug("JSON Load button clicked:" + args["myname"])

    old_basename = f["basename"]
    save(old_basename, args["renderables"], saved_data)

    new_basename = f["data_directory"] + "/" + myname 
    f["input_file"] = myname
    f["basename"] = new_basename
      
    renderables = args["renderables"]
    
    new_renderables, new_saved_data = json_load(filenames["basename"])
    saved_data.update(new_saved_data)

    for item in new_renderables.keys():
        renderables[item] = new_renderables[item]
    debug("Looks like I JSON-loaded it successfully")


def eps_callback(args):
    debug("Print to EPS button clicked")
    coords = args["eps"]["coords"]
    ps = args["eps"]["ps"]
    filename = args["eps"]["filename"]

    xvalues = [v[0] for v in coords]
    yvalues = [v[1] for v in coords]
    boundary = 10
    xmin = min(xvalues) - boundary
    ymin = min(yvalues) - boundary
    xmax = max(xvalues) + boundary
    ymax = max(yvalues) + boundary

    yc = (ymin + ymax) / 2


    headerlines = [
        "%!PS-Adobe-3.0 EPSF-3.0",
        "%%%%BoundingBox: %d %d %d %d" % (xmin, ymin, xmax, ymax),
        "%%%%HiResBoundingBox: %d %d %d %d" % (xmin, ymin, xmax, ymax),
        "%%EndComments",
        "1 setlinecap", 
        "0 %d translate" % yc,
        "1 -1 scale",
        "0 %d translate" % -yc]

    epsfile = open(filename, "w")
    for line in headerlines:
        epsfile.write(line + "\n")
    for line in ps:
        epsfile.write(line + "\n")
    epsfile.write("showpage\n")
    epsfile.close()
    #if(os.fork() == 0): # fork a child process
    #    os.execl('./showps', '')
    
def output_tikz(renderables):
    
    highlight = renderables["highlight"]
    background = renderables["background"].keys()
    matchings = renderables["matchings"]
    coords = renderables["coords"]
    dualcoords = renderables["dualcoords"]
    squishcoords = renderables["squish_coords"]
    headealines = [
        "\\begin\{tikzpicture\}[x=1pt,y=1pt]"
        ]
    footerlines = [
        "\\end\{tikzpicture\}"
        ]
    
    debug(renderables.keys())
    debug(renderables["lengths"])
    tikzfile = open("tex/tikzoutput.txt", "w")
    tikzfile.write("$$\\begin{tikzpicture}[x=1pt,y=1pt]" + "\n")
    
    for side in matchings:
        keys_list = list(side.keys()) 
        for new_edge in keys_list:
            my_edge = list(new_edge)
            if len(my_edge) != 0:
                if side == matchings[0]:
                    tikzfile.write(f"\\draw[very thick, red] {my_edge[0]} -- {my_edge[1]};" + "\n")
                else:
                    tikzfile.write(f"\\draw[very thick, blue] {my_edge[0]} -- {my_edge[1]};" + "\n")
                
    for edge in background:
        my_edge = list(edge)
        tikzfile.write(f"\\draw {my_edge[0]} -- {my_edge[1]};" + "\n")
        
    for vertex in coords:
        tikzfile.write(f"\\node at {vertex} " + "{$\\bullet$};" + "\n")
        
    for vertex in dualcoords:
        tikzfile.write(f"\\node[color = blue!50!white] at {vertex} " + "{$\\bullet$};" + "\n")
    
    if renderables["show"]["Squish"]:
        for vertex in squishcoords:
            tikzfile.write(f"\\node[color = purple!80!blue] at {vertex} " + "{$\\bullet$};" + "\n")
    
    
    tikzfile.write("\\end{tikzpicture}$$" + "\n")
    tikzfile.close()
    print("Successfully saved tikz file!")

# Toggle visibility of a layer
def showhide_callback(args):
    layer = args["layer"]
    show = args["show"]
    debug("Toggle ", layer)
    show[layer] = not show[layer]

# Toggle visibility of one of the 3 displays; resize pictures.
def showhide_picture_callback(args):
    renderables = args["renderables"]
    pictures = args["pictures"]
    show = renderables["show"]
    debug("Showing some pictures:", pictures)
    for picture in ["A", "Center", "B", "Squish"]:
        show[picture] = (picture in pictures)
    compute_picture_sizes(renderables)

def select_callback(args):
    args["show"]["Highlight"] = not args["show"]["Highlight"]  # this is whether we're in selection mode or not
    args["highlight"] = [set(), set()]
    args["selection"] = set()
    if args["show"]["Highlight"]:
        debug("Selection mode is now ON")
    else:
        debug("Selection mode is now OFF")

def angle_callback(args):
    debug("angle callback clicked")
    debug(args["i_should_add"])
    for e in args["edges"]:
        if args["i_should_add"]:
            args["matching"][e] = 1
        else:
            debug("deleting", e)
            if e in args["matching"]:
                del args["matching"][e]


def render_dimer_buttons(x,y, side, renderables, font, eps):
    buttons = []
    matchings = renderables["matchings"] 
    hexagons = renderables["hexagons"]
    highlight = renderables["highlight"]
    show = renderables["show"]
    args = {"side":side, "matching":matchings[side], "hexagons":hexagons}
    filename = eps["filename"]
    if(side==0):
        prefix = "A_"
    else:
        prefix = "B_"

    if renderables["show"]["Highlight"]:
        select_button_color = selection_color
    else:
        select_button_color = white
    buttonrow1 = [
        ("Graph", showhide_callback, {"layer":prefix+"background", "show":show}, white),
        ("Dimer", showhide_callback, {"layer":prefix+"matching", "show":show}, white),
        ("Tiling", showhide_callback, {"layer":prefix+"tiling", "show":show}, white),
        ("Boxes", showhide_callback, {"layer":prefix+"boxes", "show":show}, white),
        ("Border", showhide_callback, {"layer":prefix+"boundary", "show":show}, white),
        ("Centers", showhide_callback, {"layer":prefix+"centers", "show":show}, white),
        ("Vertices", showhide_callback, {"layer":prefix+"vertices", "show":show}, white)
        ]

    buttons =  draw_button_row(x, y, 5, font, buttonrow1)
    args["steps"]=renderables["lengths"]["randomize_steps"]
    buttonrow2 = [
        ("Randomize", randomize_callback, args, white),
        ("Minimize", minimize_callback, {"side":side,
                                         "matching":matchings[side], 
                                         "bipartition":renderables["bipartition"], 
                                         "height_changes":renderables["height_changes"], 
                                         "hexagons":hexagons}, white),
        ("Maximize", maximize_callback, {"side":side,
                                         "matching":matchings[side], 
                                         "bipartition":renderables["bipartition"], 
                                         "height_changes":renderables["height_changes"], 
                                         "hexagons":hexagons}, white),
        ("EPS", eps_callback, {"eps":eps}, white),
        ("tikz", output_tikz, renderables, white),
        ("Select", select_callback, renderables, select_button_color)
        ]
    if renderables["show"]["Highlight"] and len(renderables["selection"]) > 0:
        for angle in renderables["angles"]:
            slope = math.tan(angle)
            line = [(0,0), (round(11.0*math.cos(angle), 3), round(-11.0*math.sin(angle), 3))]
            # if line[0][1] == line[0][0]:
            #     new_line = [(line[0][0],
            #                  line[0][1] + 8), 
            #                 (line[1][0],
            #                  line[1][1] + 8)]
            # if line[1][0] < 0 and line[1][1] < 0:
            #     new_line = [(line[0][0] - line[1][0], line[0][1] - line[1][1]), (line[1][0] - line[1][0], line[1][1] - line[1][1])]
            #     label = new_line
            # elif line[1][0] < 0 and line[1][1] > 0:
            #     new_line = [(line[0][0] - line[1][0],
            #                  line[0][1]), 
            #                 (line[1][0] - line[1][0],
            #                  line[1][1])]
            #     label = new_line
            # elif line[1][0] > 0 and line[1][1] < 0:
            #     new_line = [(line[0][0], line[0][1] - line[1][1]), (line[1][0], line[1][1] - line[1][1])]
            #     label = new_line                        
            # else:
            #     label = line #str(angle) #@@@@@@@@@@@@@@@@@
            midpoint = [round((line[0][0] + line[1][0])/2.0, 3), round((line[0][1] + line[1][1])/2.0, 3)] 
            
            line[0] = (line[0][0] - midpoint[0] + 8, line[0][1] - midpoint[1] + 7)
            line[1] = (line[1][0] - midpoint[0] + 8, line[1][1] - midpoint[1] + 7)

            label = line
            callback = angle_callback,
            my_edges = renderables["angles"][angle].intersection(renderables["selection"])
            my_matching = matchings[side]
            if my_edges.issubset(my_matching):
                i_should_add = False
                my_color = selection_color
            else:
                i_should_add = True
                my_color = white
            data = {
                "matching": my_matching, 
                "edges":my_edges, 
                "i_should_add":i_should_add
            }
            buttonrow2.append((label, angle_callback, data, my_color))
    return buttons + draw_button_row(x, y+20*fourk, 5, font, buttonrow2)

def render_center_buttons(x,y,renderables, font, eps):
    buttons = []
    show = renderables["show"]
    buttonrow = [
        ("Dimer A", showhide_callback, {"layer":"center_A_matching", "show":show} , white),
        ("Border A", showhide_callback, {"layer":"center_A_boundary", "show":show} , white),
        ("Background", showhide_callback, {"layer":"center_background", "show":show} , white),
        ]
    buttonrow2 = [
        ("Dimer B", showhide_callback, {"layer":"center_B_matching", "show":show} , white),
        ("Border B", showhide_callback, {"layer":"center_B_boundary", "show":show} , white),
        ("Double edges", showhide_callback, {"layer":"center_doubled_edges", "show":show} , white),
        ("EPS", eps_callback, {"eps":eps}, white),
        ("tikz", output_tikz, renderables, white)
        ]
    buttons =  draw_button_row(x, y, 5, font, buttonrow)
    return buttons+draw_button_row(x, y+20*fourk, 5, font, buttonrow2)

def render_os_buttons(x, y, filenames, renderables, font):
    buttons = []
    datadir = filenames["data_directory"]
    files = sorted(os.listdir(datadir))
    buttonrow = [
        ("Quit", quit_callback, {}, white),
    ]
    buttonrow.extend([( f, 
                        json_load_callback, 
                        {   "myname":f, 
                            "filenames":filenames, 
                            "renderables":renderables,
                            "saved_data": saved_data}, white) for f in files])
    return draw_button_row(x,y,5,font,buttonrow)

def render_showhide_buttons(x,y, renderables, font):
    buttonrow = [
        ("Just A", showhide_picture_callback, {"pictures":["A"], "renderables":renderables} , white),
        ("A union B", showhide_picture_callback,{"pictures":["Center"], "renderables":renderables} , white),
        ("Just B", showhide_picture_callback, {"pictures":["B"], "renderables":renderables} , white),
        ("A beside B", showhide_picture_callback,{"pictures":["A","B"], "renderables":renderables} , white),
        ("All three", showhide_picture_callback,{"pictures":["A","Center", "B"], 
                "renderables":renderables} , white),
        ("Clear highlight", clear_highlight_callback,{"renderables":renderables} , white),
        ("Squish", showhide_picture_callback, {"pictures":["A", "Squish"], "renderables":renderables}, white)

        #("Full screen", fullscreen_callback, {"renderables":renderables}),
        
    ]
    return draw_button_row(x,y,5,font,buttonrow)

def render_global_adjusters(x,y, renderables, font):
    adjusterlist = [
        ("Dimer width", "dimer_width", 1, white),
        ("Center radius", "hex_flipper_radius", 1, white),
        ("Overlay offset", "overlay_offset", 1, white),
        ("Tile edge", "tile_edge_width", 1, white),
        ("Shading intensity", "shading_intensity", 0.1, white),
        ("Randomize steps", "randomize_steps", 100, white),
        ("Squish Ratio", "squish_ratio", 1, white)
    ]
    return draw_adjuster_row(x,y,5,renderables["lengths"],font,  adjusterlist)

#=============================================================
# Draw everything.  Return a list of clickable boxes.
def render_everything(renderables,filenames, font):
    background = renderables["background"] 
    matchings = renderables["matchings"] 
    hexagons = renderables["hexagons"]
    rhombi = renderables["rhombi"]
    coords = renderables["coords"]
    squish_coords = renderables["squish_coords"]
    dualcoords = renderables["dualcoords"]
    show = renderables["show"]
    lengths = renderables["lengths"]
    bipartition = renderables["bipartition"]

    current_file = filenames["input_file"]
     
    y = lengths["y"]*fourk
    xA =lengths["xA"]
    xB = lengths["xB"]
    xCenter = lengths["xCenter"]
    window = lengths["window"]

    screen.fill(white)
    
    boxes = []

    if show["A"]:
        epsA = {"coords":[], "ps":[], "filename":current_file + "_A.eps"}
        if show["A_background"]:
            boxes += render_background(renderables, xA, y, 0, epsA)
        if show["A_boxes"]:
            render_boxes(renderables,0,xA, y, A_color, epsA)
        if show["Highlight"]:
            render_highlight(renderables,0, xA, y, hi_color, epsA)
            render_selection(renderables,0, xA, y, selection_color, epsA)
        if show["A_matching"]:
            render_matching(renderables,0, xA, y, A_color, epsA)
        if show["A_boundary"]:
            render_boundary(renderables,  0, xA, y, A_color, epsA)
        if show["A_tiling"]:
            render_tiling(renderables,0, xA, y, A_color, epsA)
        if show["A_centers"]:
            boxes += render_active_centers(renderables, xA, y, 0,epsA)
        if show["A_vertices"]:
            render_matched_vertices(renderables, 0, xA, y, A_color, epsA)
        boxes += render_dimer_buttons(10+xA, 10, 0, renderables, font, epsA)
        renderables["epsA"] = epsA
        if eps_only: # hack to render eps programattically
            eps_callback({"eps":epsA})

    if show["B"]:
        epsB = {"coords":[], "ps":[], "filename": current_file + "_B.eps"}
        if show["B_background"]:
            boxes += render_background(renderables, xB, y, 1, epsB)
        if show["B_boxes"]:
            render_boxes(renderables,1,xB, y, B_color, epsB)
        if show["Highlight"]:
            render_highlight(renderables,1, xB, y, hi_color, epsB)
            render_selection(renderables,1, xB, y, selection_color, epsB)
        if show["B_matching"]:
            render_matching(renderables,1, xB, y, B_color, epsB)
        if show["B_boundary"]:
            render_boundary(renderables, 1, xB, y, B_color, epsB)
        if show["B_tiling"]:
            render_tiling(renderables, 1, xB, y, B_color, epsB)
        if show["B_centers"]:
            boxes += render_active_centers(renderables, xB, y, 1, epsB)
        if show["B_vertices"]:
            render_matched_vertices(renderables, 1, xB, y, B_color, epsB)
        boxes += render_dimer_buttons(10+xB, 10, 1, renderables, font, epsB)
        renderables["epsB"] = epsB
        if eps_only: # hack to render eps programattically
            eps_callback({"eps":epsB})
        
    if show["Center"]:
        epsCenter = {"coords":[], "ps":[], "filename":current_file + "_AB.eps"}
        if show["center_background"]:
            render_background(renderables, xCenter, y, 1, epsCenter)
        #if show["Highlight"]:
        #    render_highlight(renderables,0, xCenter, y, hi_color, epsCenter)
        #    render_highlight(renderables,1, xCenter, y, hi_color, epsCenter)
        #    render_temp_selection(renderables,1, xCenter, y, selection_color, epsCenter)
            
        if show["center_A_boundary"]:
            render_boundary(renderables, 0, xCenter+lengths["overlay_offset"], y, A_color, epsCenter)
        if show["center_B_boundary"]:
            render_boundary(renderables, 1, xCenter-lengths["overlay_offset"], y, B_color, epsCenter)
        if show["center_A_matching"] and not show["center_B_matching"]:
            boxes += render_matching(renderables,0, xCenter, y, A_color, epsCenter)
        if show["center_B_matching"] and not show["center_A_matching"]:
            boxes += render_matching(renderables,1, xCenter, y, B_color, epsCenter)
        if show["center_A_matching"] and show["center_B_matching"]:
            boxes += render_xor_edges(renderables, xCenter, y, [A_color, B_color], epsCenter)
            if show["center_doubled_edges"]: 
                render_doubled_edges(renderables, xCenter, y, [A_color, B_color], epsCenter)
        boxes += render_center_buttons(10*fourk+xCenter, 10*fourk, renderables, font, epsCenter)
        renderables["epsCenter"] = epsCenter
        if eps_only: # hack to render eps programattically
            eps_callback({"eps":epsCenter})
            
    if show["Squish"]:
        epsA = {"coords":[], "ps":[], "filename":current_file + "_A.eps"}
        xthtime = 0
        if show["A_background"] and xthtime == 0:
            boxes += render_squish_background(renderables, xB, y, 0, epsA)
        if show["A_boxes"]:
            render_boxes(renderables, 0, xB, y, Squish_color, epsA)
        if show["Highlight"]:
            render_highlight(renderables, 0, xB, y, hi_color, epsA)
        if show["A_matching"]:
            render_squish_matching(renderables, 0, xB, y, Squish_color, epsA)
        if show["A_boundary"]:
            render_boundary(renderables, 0, xB, y, Squish_color, epsA)
        if show["A_tiling"]:
            render_tiling(renderables, 0, xB, y, Squish_color, epsA)
        #if show["A_fpl"]:
        #    render_fpl(renderables,0, xB, y, Squish_color, epsA)
        if show["A_centers"]:
            boxes += render_active_centers(renderables, xB, y, 0,epsA)
        boxes += render_dimer_buttons(10+xB, 10, 0, renderables, font, epsA)
        renderables["epsA"] = epsA
        if eps_only: # hack to render eps programattically
            eps_callback({"eps":epsA})

    y = lengths["screen_height"] - lengths["button_height"]*fourk
    
    boxes += render_showhide_buttons(10,y, renderables, font)
    boxes += render_global_adjusters(400*fourk, y, renderables, font)
    y -= lengths["button_height"]*fourk
    boxes += render_os_buttons(10,y,filenames,renderables,font)

    if eps_only:
        bigfont = pygame.font.Font(None, 72)
        text = bigfont.render("Creating eps... please wait", True, black, grey)    
        bb = text.get_rect()
        bb.center = (lengths["screen_width"]/2, lengths["screen_height"]/2)
        screen.blit(text, bb)


    pygame.display.flip()
    return boxes

#==============================================================
# make an adjacency map from a matching. 
def adjacency_map(M): 
    adj = {}
    for edge in M:
        endpoints = [endpt for endpt in edge]
        adj[endpoints[0]] = endpoints[1]
        adj[endpoints[1]] = endpoints[0]
    return adj

#==============================================================
# Can we flip a matching around a hexagon? 
def is_active(face, frozen_matching):
    acc = 0
    for e in face:
        work = frozenset(tuple(u) for u in e)
        if work in frozen_matching:
            acc += 1
    active = (2*acc == len(face))
    return active

def is_hf_minimum(face, frozen_matching, bipartition):
    print("starting hf minimum testing", face)
    if not is_active(face, frozen_matching):
        return False
    for e in face:
        frozen_edge = frozenset([tuple(v) for v in e])
        if frozen_edge in frozen_matching:
            break
    poly = edge_list_to_polygon(face)
    if not is_positively_oriented(poly):
        poly = list(reversed(poly))
    a,b = [tuple(u) for u in e]
    if bipartition[b] == 0: # make sure a is in the first part of bipartition
        (a,b) = (b,a)    
    i = poly.index(a)
    j = poly.index(b)
    print("ordered edge in question", (a,b))
    print("polygon testing", poly)
    # check whether i is before j in cyclic order 
    if (j-i) % len(face)  == 1:
        return False
    elif (i-j) % len(face) == 1:
        return True
    else:
        raise Exception("something is weird")


     
#=============================================================
# Return a list of the hexagons adjacent to a given hexagon
# Some of these might not exist in the graph
#
#              H                               
#                                              
#        H   X---X   H                         
#           /     \                            
#          X   H   X                           
#           \     /                            
#        H   X---X   H                         tuple(tuple(tuple(
#                                              
#              H                               

def hex_neighbors(hexagon):
    (r,c) = hexagon
    return [(r-4,c), (r-2, c+6), (r+2,c+6), (r+4,c), (r+2,c-6), (r-2,c-6)]

def freeze_matching(M):
    result = []
    for edge in M:
        result.append(frozenset(tuple(u) for u in edge))
    return frozenset(result)

#==============================================================
def render_active_centers(renderables, xoffset, yoffset, 
            which_side, eps):
    coords=renderables["coords"] 
    hexlist=renderables["hexagons"] 
    matching=renderables["matchings"][which_side]
    lengths=renderables["lengths"]

    boxes = []
    frozen = freeze_matching(matching)
    debug("Hex center rendering starting now")
    for i in range(len(hexlist)):
        h = hexlist[i]
        #debug(h)
        if is_active(h, frozen): 
            bb = render_hexagon_center(coords, h, xoffset, yoffset, lengths["hex_flipper_radius"], eps)
            boxes.append(("hexagon", i, which_side, bb))
    return boxes

       
             
#==============================================================
# Find a path in the superposition of two matchings, starting at
# a point in the first matching.  Might be a loop.
# The matchings should be given as adjacency maps (see adjacency_map)
# The path is returned as a list of vertices.  If the path is closed then
# the starting vertex is repeated.
def find_path(adj1, adj2, start):
    path = [start];
    p1 = start;
    try:
        p2 = adj2[p1]
        path.append(p2)
        p1 = adj1[p2]
        while(p1 != start):
            path.append(p1)
            p2 = adj2[p1]
            path.append(p2)
            p1 = adj1[p2]
        path.append(p1)
        return path
    except KeyError: 
        pass

# We fall through to here if path isn't closed.  Get the rest of the path.

    p1 = start
    try:  
        p2 = adj1[p1]
        path.insert(0,p2)
        p1 = adj2[p2]
        while(p1 != start):
            path.insert(0,p1)
            p2 = adj1[p1]
            path.insert(0,p2)
            p1 = adj2[p2]

    except KeyError: 
        pass

    return path

# Flip a path in two matchings, starting at an edge in the first.
def flip_path(matchings, m1, m2, unordered_edge):
    edge = [endpoint for endpoint in unordered_edge]
    adj1 = adjacency_map(matchings[m1])
    adj2 = adjacency_map(matchings[m2])
    path = find_path(adj1, adj2, edge[0])

    # don't do anything if user clicked a doubled edge.
    if(len(path) == 3 and path[0] == path[2]): return 

    # make lists of edges corresponding to the path
    loop1 = []
    loop2 = []
    for i in range(len(path) - 1):
        e = frozenset([path[i], path[i+1]])
        if e in matchings[m1]: loop1.append(e)
        if e in matchings[m2]: loop2.append(e)
    for e in loop1:
        del matchings[m1][e]
        matchings[m2][e] = 1
    for e in loop2:
        del matchings[m2][e]
        matchings[m1][e] = 1

#============================================================================
# Flip one hexagon.
def flip_hex(matching, hexagons, index):
    h = hexagons[index]
    for e in h:
        tuple_edge = frozenset(tuple(v) for v in e)
        if(tuple_edge in matching): 
            del matching[tuple_edge]
        else:
            matching[tuple_edge] = 1


#====================================================================
# Run the glauber dynamics to randomize one picture
def randomize(matching, hexlist, steps):
    for trial in range(steps):
        frozen = freeze_matching(matching)
        # Choose a random index i
        activelist = []
        for i in range(len(hexlist)):
            if is_active(hexlist[i],frozen):
                activelist.append(i)

        if len(activelist) > 0:
            print("active hexes:", activelist)
            i = random.choice(activelist)
            print(i, hexlist[i], is_active(hexlist[i], frozen))
            flip_hex(matching, hexlist, i) 
        else:
            debug("no active hex centers")
            break

def maximize(matching, bipartition, height_changes, hexlist):
    finished = False
    while not finished:
        frozen = freeze_matching(matching)
        activelist = []
        for i in range(len(hexlist)):
            if is_hf_minimum(hexlist[i], frozen, bipartition):
                activelist.append(i)
        if len(activelist) == 0:
            finished = True
        else:
            for i in activelist:
                flip_hex(matching, hexlist, i)


def minimize(matching, bipartition, height_changes, hexlist):
    finished = False
    while not finished:
        frozen = freeze_matching(matching)
        activelist = []
        for i in range(len(hexlist)):
            if is_active(hexlist[i], frozen) and not is_hf_minimum(hexlist[i], frozen, bipartition):
                activelist.append(i)
        if len(activelist) == 0:
            finished = True
        else:
            for i in activelist:
                flip_hex(matching, hexlist, i)



#====================================================================
# Find the maximum matching, with respect to height function.
# This is broken.  New code needs to be lattice-independent.
#def maximize(matching, hexlist):
#    activelist = []
#    finished = False
#    while not finished:
#        finished = True
#        for i in range(len(hexlist)):
#            h = hexlist[i]
#            e = [frozenset([h[i], h[(i+1)%6]]) for i in range(6)]
#            if e[0] in matching and e[2] in matching and e[4] in matching:
#                del matching[e[0]]
#                del matching[e[2]]
#                del matching[e[4]]
#                matching[e[1]] = 1
#                matching[e[3]] = 1
#                matching[e[5]] = 1
#                finished = False

#====================================================================
# Find the aspect ration of one of our pictures.  
def aspectratio(coords):
    xvalues = [v[0] for v in coords.values()]
    yvalues = [v[1] for v in coords.values()]
    xmin = min(xvalues) + 0.0
    ymin = min(yvalues) + 0.0
    xmax = max(xvalues) + 0.0
    ymax = max(yvalues) + 0.0
    return (xmax-xmin)/(ymax-ymin)

#====================================================================
# Find the minimum matching, with respect to height function.
#def minimize(matching, hexlist):
#    activelist = []
#    finished = False
#    while not finished:
#        finished = True
#        for i in range(len(hexlist)):
#            h = hexlist[i]
#            e = [frozenset([h[i], h[(i+1)%6]]) for i in range(6)]
#            if e[1] in matching and e[3] in matching and e[5] in matching:
#                del matching[e[1]]
#                del matching[e[3]]
#                del matching[e[5]]
#                matching[e[0]] = 1
#                matching[e[2]] = 1
#                matching[e[4]] = 1
#                finished = False

#=================================================
# Compute coordinates of dual vertices.
def dualcoords(hexlist):
    dualvertexlist = []
    for h in hexlist:
        x = 0;
        y = 0;
        for p in h:
            x += p[0];
            y += p[1];
        center = (x/6, y/6);    
        dualvertexlist.append(center);
    return dualvertexlist;


#====================================================================
def save(basename, renderables, saved_data):
    matchings = renderables["matchings"]
    
    saved_data["matchings"] = []
    for m in matchings:
        m_as_list = [list(e) for e in m.keys()]
        saved_data["matchings"].append(m_as_list)
    saved_data["show"] = renderables["show"]
    saved_data["lengths"] = renderables["lengths"]

    with open(basename, "w") as outputfile:
        json.dump(saved_data, outputfile)

    #show = renderables["show"]
    #lengths = renderables["lengths"]
    #write_edges(matchings[0], basename + "A.edge")
    #write_edges(matchings[1], basename + "B.edge")
    #showfile = open(basename + "show.pkl", "wb")
    #pickle.dump(show, showfile)
    #showfile.close()
    #lengthsfile = open(basename + "lengths.pkl", "wb"))
    #pickle.dump(lengths, lengthsfile)
    #lengthsfile.close()

#===================================================================
# Compute the proper scale and locations of pictures, based on the
# screen size and all the things we need to draw.
# Note: I do a whole bunch of making sure various lengths are at least 100, to stop divide by zero errors
# there is no particular reason why 100 was the number to use here.  The app is useless at such a low resolution anyway

def compute_picture_sizes(renderables):
    
    horiz_padding = 10
    lengths = renderables["lengths"]
    show = renderables["show"]
    picturecount = 0
    if show["A"]: picturecount += 1
    if show["B"]: picturecount += 1
    if show["Squish"]: picturecount += 1 # squish map
    if show["Center"]: picturecount += 1

    screensize = screen.get_size()

    height = screensize[1] - 5*lengths["button_height"]
    width = screensize[0]
    

    if height < 100:
        height = 100
    if width < 100:
        width = 100

    window = width
    if picturecount > 0:
        window = int(screensize[0] / picturecount)
    aspect = aspectratio(renderables["dualcoords"])
    if int(height * aspect) < window:
        window = int(height * aspect)

    if window < 100:
        window = 100

    lengths["screen_width"] = max(screensize[0], 100)
    lengths["screen_height"] = max(screensize[1], 100)
    lengths["window"] = window
    

# Compute x coordinates at which to draw pictures
    lengths["xA"] = int((width - window * picturecount + picturecount*horiz_padding)/2)
    if(show["A"]):
        lengths["xCenter"] = lengths["xA"] + window + horiz_padding*fourk
    else:
        lengths["xCenter"] = lengths["xA"] + horiz_padding*fourk
    if(show["Center"]):
        lengths["xB"] = lengths["xCenter"] + window + horiz_padding*fourk
    else:
        lengths["xB"] = lengths["xCenter"] + horiz_padding*fourk

    renderables["coords"] = copy.deepcopy(renderables["unscaled_coords"])
    renderables["dualcoords"] = copy.deepcopy(renderables["unscaled_dualcoords"])
    renderables["squish_coords"] = copy.deepcopy(renderables["unscaled_squish_coords"])
    rescale(renderables["coords"], renderables["dualcoords"], window-horiz_padding, renderables["squish_coords"])


def read_height_changes_json(height_changes):
    height_change_dict = {}
    for (e, delta_h) in height_changes:
        edge = frozenset((tuple(e[0]), tuple(e[1])))
        height_change_dict[edge] = height_changes
    return height_change_dict

#=================================================================
# JSON stuff

def json_load(basename):
    if not os.path.exists(basename):
        exit("JSON: Can't find "+basename)

    with open(basename, "r") as data_file:
        saved_data = json.load(data_file)

    coords = read_vertices_json(saved_data["vertices"])
    unscaled_coords = copy.deepcopy(coords)
    squish_coords = squish(coords)
    unscaled_squish_coords = copy.deepcopy(squish_coords)
    background = read_edges_json(saved_data["edges"])
    faces = read_faces_json(saved_data["faces"])
    dualcoords = read_vertices_json(saved_data["dual_vertices"])
    unscaled_dualcoords = copy.deepcopy(dualcoords)
    rhombi = read_tiles_json(saved_data["tiles"]) #saved_data["tiles"]
    bipartition = read_bipartition_json(saved_data["bipartition"])
    angles = read_angles_json(saved_data["angles"])
    height_changes = read_height_changes_json(saved_data["height_changes"])
    
    
    if "matchings" in saved_data:
        matchings = read_matchings_json(saved_data["matchings"])
    else:
        matching_A = {} 
        matching_B = {} 
        matchings = [matching_A, matching_B]

    if "show" in saved_data:
        show = saved_data["show"]
    else:
        show = {
            "A": True,
            "B": True,
            "Center": False,
            "Highlight": False,
            "Squish": False,

            "A_background": True,
            "A_matching": True,
            "A_tiling": False,
            "A_boundary": False,
            "A_centers": False,
            "A_boxes": False,
            "A_vertices": False,

            "B_background": True,
            "B_matching": True,
            "B_tiling": False,
            "B_boundary": False,
            "B_centers": False,
            "B_boxes": False,
            "B_vertices": False,

            "center_background": False,
            "center_A_matching": False,
            "center_B_matching": False,
            "center_A_boundary": False,
            "center_B_boundary": False,
            "center_doubled_edges": False,
        }
    
    if "lengths" in saved_data:
        lengths = saved_data["lengths"]
    else:
        lengths = {}

    default_lengths = {
        "button_height": 20,
        "dimer_width":3,
        "hex_flipper_radius":4,
        "overlay_offset":0,
        "tile_edge_width":2,
        "shading_intensity":1,
        "randomize_steps":100,
        "y": 60,
        "squish_ratio":1
        }

    for param in default_lengths.keys():
        if param not in lengths:
            lengths[param] = default_lengths[param]
    # this allows us to add new keys
    show_default_dict = defaultdict(bool)
    show_default_dict.update(show)

    renderables = {"highlight":[{},{}], # highlighted edges on left and right
                   "background": background, 
                   "matchings": matchings, 
                   "hexagons": faces, 
                   "rhombi": rhombi,
                   "coords": coords,
                   "unscaled_coords": unscaled_coords,
                   "squish_coords": squish_coords,
                   "unscaled_squish_coords": squish_coords,
                   "dualcoords": dualcoords,
                   "unscaled_dualcoords": unscaled_dualcoords,
                   "show": show_default_dict,
                   "selection":set(),
                   "lengths":lengths,
                   "bipartition":bipartition,
                   "height_changes":height_changes,
                   "angles":angles}
    compute_picture_sizes(renderables)
    return renderables, saved_data


#===================================================================
# Highlighting code
def highlight_edge(renderables, edge, side):
    highlight = renderables["highlight"]
    if edge in highlight[side]:
        #debug("edge highlight off: side ", side, edge)
        del highlight[side][edge]
    else:
        #debug("edge highlight on: side ", side, edge)
        highlight[side][edge] = 1

def highlight_hexagon(renderables, hexagon, side):
    highlight = renderables["highlight"]
    edges = []
    for i in range(6):
        j = (i+1) % 6
        edges.append(frozenset([hexagon[i], hexagon[j]]))
    all_highlighted = True

    for edge in edges:
        if not(edge in highlight[side]):
            all_highlighted = False

    if(all_highlighted):
        for edge in edges:
            del highlight[side][edge]
    else:
        for edge in edges:
            highlight[side][edge] = 1 

def highlight_path(renderables, m0, m1, unordered_edge):
    matchings = renderables["matchings"]
    edge = [endpoint for endpoint in unordered_edge]
    adj0 = adjacency_map(matchings[m0])
    adj1 = adjacency_map(matchings[m1])
    path = find_path(adj0, adj1, edge[0])

    #list of edges corresponding to the path
    loop = []
    all_highlighted = True

    for i in range(len(path) - 1):
        e = frozenset([path[i], path[i+1]])
        loop.append(e)
        if e not in renderables["highlight"][m0]: 
            all_highlighted = False
        if e not in renderables["highlight"][m1]:
            all_highlighted = False

    if all_highlighted:
        for e in loop:
            del renderables["highlight"][m0][e]
            del renderables["highlight"][m1][e]
    else:
        for e in loop:
            renderables["highlight"][m0][e] = 1
            renderables["highlight"][m1][e] = 1
 

#===================================================================
# Main program


pygame.init()
pygame.font.init()
font = pygame.font.Font(None, 18*fourk)
screen=pygame.display.set_mode((1364,690), pygame.RESIZABLE)

#pygame.mouse.set_cursor(*pygame.cursors.broken_x)


try:
    data_directory = sys.argv[1]
except IndexError:
    data_directory = "json"

try:
    input_file = sys.argv[2]
except IndexError:
    possible_start_files = os.listdir(data_directory)
    input_file = possible_start_files[0]

eps_only = False
try:
    switch = sys.argv[3]
    if switch == "eps":
        eps_only = True
except IndexError:
    pass

filenames = {   "data_directory":data_directory,
                "input_file":input_file,
                "basename":data_directory + "/" + input_file } 

renderables, saved_data = json_load(filenames["basename"])

bounding_box_data = render_everything(renderables,filenames,font)
bounding_boxes = [record[3] for record in bounding_box_data]

done=False     #Loop until the user clicks the close button....
if eps_only:   # unless we're just rendering the eps files, which has already been done.
    done=True

clock=pygame.time.Clock() # Used to manage how fast the screen updates


# Code for highlighting paralellogram
selection = {}
selecting_parallelogram = False
first_corner = None
second_corner = None
def parallel(e1, e2):
    delta_c = []
    delta_r = []
    for e in [e1, e2]:
        [(r1, c1),(r2,c2)] = list(e) 
        if c2>c1:
            delta_c.append(c2-c1)
            delta_r.append(r2-r1)
        else:
            delta_c.append(c1-c2)
            delta_r.append(r1-r2)
    return (delta_c[0] == delta_c[1] and delta_r[0] == delta_r[1])

# Determine the two "increment vectors" which are the directions 
# of the two sides of the paralellogram we're trying to select
def find_increments(e1, e2):
    [(r1, c1),(r2,c2)] = list(e1[0]) 
    if c2>c1:
        ((r1,c1),(r2,c2)) = ((r2,c2),(r1,c1))
    [(r21, c21),(r22,c22)] = list(e2[0])
    if c22>c21:
        ((r21,c21),(r22,c22)) = ((r22,c22),(r21,c21))
    delta_c=(c2-c1)
    delta_r=(r2-r1)
    (vr,vc) = (r21-r1, c21-c1) 

    if (delta_r, delta_c) in ((0,4), (0,-4)): # "---"
        ((a,c),(b,d)) = ((-2, -6), (-2, 6)) 
    elif (delta_r, delta_c) in ((2,2), (-2,-2)): # "\"
        ((a,c), (b,d)) = ((4,0), (-2,-6))
    elif (delta_r, delta_c) in ((-2,2), (2,-2)): # "/"
        ((a,c), (b,d)) = ((-2,6), (4,0))
    else:
        raise Exception("Something weird happened: %d, %d" % (delta_r, delta_c))
    (inc0, inc1) = ((a,c),(b,d))
    det = a*d - b*c
    max0 = ( d*vr - b*vc)/det
    max1 = (-c*vr + a*vc)/det
    return (inc0, inc1, max0, max1)

def find_selected_edges(e1, e2, renderables):
    (inc0, inc1, max0, max1) = find_increments(e1,e2)
    graph = renderables["rhombi"]
    if max0 < 0:
        max0 *= -1
        inc0 = (-inc0[0], -inc0[1]) 
    if max1 < 0:
        max1 *= -1
        inc1 = (-inc1[0], -inc1[1]) 
    (v0, v1) = tuple(e1[0])
    select = set()
    for i in range(max0+1):
        for j in range(max1+1):
            hi_v0 = (v0[0] + i*inc0[0] + j*inc1[0], v0[1] + i*inc0[1] + j*inc1[1])
            hi_v1 = (v1[0] + i*inc0[0] + j*inc1[0], v1[1] + i*inc0[1] + j*inc1[1])
            hi_e = frozenset([hi_v0, hi_v1])
            if hi_e in graph:
                select.add(hi_e)
    return select

    
#=================================================
# Pygame event loop
which_button_clicked = None
while done==False:
    for event in pygame.event.get(): # User did something
        select_mode = renderables["show"]["Highlight"] 
        if event.type == pygame.QUIT: # If user clicked close
            done=True # Flag that we are done so we exit this loop
        if event.type == pygame.VIDEORESIZE:
            debug("user resized screen ", event.size)
            #pygame.time.wait(1000)
            #screen=pygame.display.set_mode(event.size, pygame.RESIZABLE)
            #pygame.time.wait(1000)
            debug("resizing done")
            compute_picture_sizes(renderables)
            bounding_box_data = render_everything(renderables, filenames,font)
            bounding_boxes = [record[3] for record in bounding_box_data]
        
        
        if event.type == pygame.MOUSEBUTTONDOWN:
            # Are we on a button? If so make note of which one
            radius = 1
            ul = (event.pos[0] - radius, event.pos[1] - radius)
            clickpoint = pygame.Rect(ul , (2*radius, 2*radius))
            box_index = clickpoint.collidelist(bounding_boxes)
            if(box_index != -1):
                objtype = bounding_box_data[box_index][0]
                if objtype == "button":
                    (objtype, args, callback, bb) = bounding_box_data[box_index]
                    which_button_clicked = bb
                    debug("mousedown over button: ", bb)
 
            if select_mode:
                brush_positions = [(event.pos[0], event.pos[1])]


 
 
        if event.type == pygame.MOUSEBUTTONUP and select_mode:
            new_edges = []
            if len(brush_positions) >= 3:
                lasso = shapely.geometry.Polygon(brush_positions)
                bounding_box_data = render_everything(renderables, filenames, font)
                bounding_boxes = [record[3] for record in bounding_box_data]
                for (boxtype, e, side, bb) in bounding_box_data:
                    if boxtype == "edge":
                        p = shapely.geometry.Point(bb.center)
                        if p.within(lasso):
                            new_edges.append(e)
            renderables["selection"].update(new_edges)
            brush_positions = []

            radius = 1
            ul = (event.pos[0] - radius, event.pos[1] - radius)
            clickpoint = pygame.Rect(ul , (2*radius,2*radius))
            box_index = clickpoint.collidelist(bounding_boxes)
            if(box_index != -1):
                objtype = bounding_box_data[box_index][0]
                if objtype == "button":
                    (objtype, args, callback, bb) = bounding_box_data[box_index]
                    debug("mouseup over button in select mode", bb)
                    if bb==which_button_clicked:
                        callback(args)
            bounding_box_data = render_everything(renderables, filenames, font)
            bounding_boxes = [record[3] for record in bounding_box_data]
            which_button_clicked = None

        if event.type == pygame.MOUSEMOTION and select_mode and pygame.mouse.get_pressed()[0]:
            #brush_size = 10
            mouse_position = (event.pos[0], event.pos[1])
            pygame.draw.line(screen, hi_color, brush_positions[-1], mouse_position, width=1)
            pygame.display.flip()
            brush_positions.append(mouse_position)
            
        if event.type == pygame.MOUSEBUTTONUP and not select_mode:
            radius = 1
            ul = (event.pos[0] - radius, event.pos[1] - radius)
            clickpoint = pygame.Rect(ul , (2*radius,2*radius))
            box_index = clickpoint.collidelist(bounding_boxes)
            if(box_index != -1):
                objtype = bounding_box_data[box_index][0]
                matchings = renderables["matchings"]
                hexagons = renderables["hexagons"]
                if objtype == "edge":
                    (objtype, edge, side, box) = bounding_box_data[box_index]
                    
                    if(event.button == 1):
                        if edge in matchings[side]:
                            debug("deleting")
                            del matchings[side][edge]
                        else:
                            debug("adding")
                            matchings[side][edge] = 1
                elif objtype == "matchededge":
                    (objtype, matchededge, side, box) = bounding_box_data[box_index]
                    if event.button == 3:
                        highlight_path(renderables, side, 1-side, matchededge)
                    else:
                        flip_path(matchings, side, 1-side, matchededge)
                elif objtype == "button":
                    (objtype, args, callback, bb) = bounding_box_data[box_index]
                    debug("mouseup over button, NOT in select mode", bb)
                    if bb==which_button_clicked:
                        callback(args)
                    else:
                        debug("not equal", bb, which_button_clicked)
                elif objtype == "hexagon":
                    (objtype, hexagon, side, box) = bounding_box_data[box_index]
                    if event.button == 3:
                        highlight_hexagon(renderables, hexagons[hexagon], side)
                    else:
                        flip_hex(matchings[side], hexagons, hexagon)
                else:
                    debug("uh why am I here?")

                bounding_box_data = render_everything(renderables, filenames, font)
                bounding_boxes = [record[3] for record in bounding_box_data]
                which_button_clicked=None
    # Limit to 100 frames per second UNLESS we're just waving the mouse around
    if event.type in {pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP}:
        clock.tick(10)
 
pygame.font.quit()
pygame.quit()

if not eps_only:
    save(filenames["basename"], renderables, saved_data)


